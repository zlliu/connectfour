'''
Implement the class 'ConnectFourBoard'.
'''
from Board import *

class ConnectFourBoardError(Exception):
    pass

class FullColumnError(ConnectFourBoardError):
    pass


class ConnectFourBoard:
    '''
    This class implements a Connect Four board, and handles basic operations.
    State of the board at one location is represented by a character:
    Use 'o' to represent disc of player1;
    Use 'x' to represent disc of player2;
    Use '|' to represent empty location.
    
    Reasoning about this class:
    This class builds on 'Board' class.  It uses an instance of 'Board' to 
    represent a ConnectFour board.  And it maintains the board, making sure
    users don't mess up with the board.  It has an internal state to keep track
    of whose turn it is, and either adds a disc to a column, or removes the last 
    disc.
    Essentially, this class creates an abstraction layer on top of 'Board'.
    It protect 'Board', and users of this class need to know nothing about 
    'Board'.
    That's why I create a mapping between input state and output state, even 
    though they are set the same.
    '''
    def __init__(self, row = 6, col = 7):
        '''
        Constructor: initialize internal state variables.
        '''        
        self.board = Board(row, col)
        # 'available_moves[i]' gives next available location(index) at column i.
        # If the value is self.board.get_row, then the column is full.
        # TODO: How about make 'available_moves' a class to enforce the validility
        # of its contents. (between 0 and number of rows)
        self.available_moves = [0] * col
        self.player1_turn = True  # By default, Player 1 goes first.
        # A list of column indices of the all discs that have been made.
        self.col_made = []  # no discs have been added.
        # The state value from 'Board' class.
        # Dictionary values are chosen based on how 'Board' represents board
        # state, described in class docstring.
        self.__in_board_state = {'player1' : 'o', 'player2' : 'x', \
                                 'empty' : '|'}
        # The state value exposed to users of this class.  Dictionary values
        # can be changed.
        self.__out_board_state = {'player1' : 'o', 'player2' : 'x', \
                                  'empty' : '|'}
        
    def get_col(self):
        '''
        Return the number of columns in the board.
        '''
        return self.board.get_col()
    
    def get_row(self):
        '''
        Return the number of rows in the board.
        '''
        return self.board.get_row()
    
    def get_board_state(self, row, col):
        '''
        Return the state of the ConnectFour board at given row and column.
        '''
        if row not in range(self.board.get_row()):
            raise ConnectFourBoardError("Incorrect row index.")
        if col not in range(self.board.get_col()):
            raise ConnectFourBoardError("Incorrect column index.")
        if self.board[row, col] == self.__in_board_state["player1"]:
            return self.__out_board_state["player1"]
        if self.board[row, col] == self.__in_board_state["player2"]:
            return self.__out_board_state["player2"]
        return self.__out_board_state["empty"]
    
    #TODO: Following method may be better moved outside to the caller.  
    # Or, if it's not called outside, we should just remove it.
    def get_available_columns(self):
        '''
        Get non-full columns on the current board.
        Argument: None
        Return:
        -- columns: a list of integer (column indices)
        '''
        columns = []
        for (col, row) in enumerate(self.available_moves):
            if row < self.board.get_row():
                columns.append(col)
        return columns
    
    #TODO: Following method may be better moved outside to the caller.    
    def get_top_location(self, col):
        '''
        Get the row index of the first empty location from bottom for given
        column 'col'.
        Argument:
        -- col: integer
        Return:
        -- integer
        '''
        row = self.available_moves[col]
        if row == self.board.get_row():
            raise ConnectFourBoardError("Given column is full.")
        return row
    
    def get_current_player(self):
        '''
        Return 'player1' if self.player1_turn is True; and 'player2' if False.
        '''
        if self.player1_turn:
            return 'player1'
        return 'player2'

    # Mutators.
    def add_to_column(self, col):
        '''
        Add a disc to a given column.
        Argument:
        -- col: integer
        Return: None
        '''
        if col not in range(self.board.get_col()):
            raise ConnectFourBoardError("Incorrect column index.")
        row = self.available_moves[col]
        if row == self.board.get_row():
            raise FullColumnError("Given column is full.")
        
        if self.player1_turn:
            # Write to 'self.board'.
            self.board[row, col] = self.__in_board_state['player1']
            # Switch to player2.
            self.player1_turn = False
        else:
            self.board[row, col] = self.__in_board_state['player2']
            # Switch to player1.
            self.player1_turn = True
        # Update other attributes.
        assert(row + 1 <= self.board.get_row())
        self.available_moves[col] = row + 1
        self.col_made.append(col)
        
    def remove_last_disc(self):
        '''
        Remove the last added disc.
        Argument: None
        Return: None
        '''
        if self.col_made == []:
            raise ConnectFourBoardError("No discs on current board.")
        last_col = self.col_made.pop() # Remove and return the last item.
        row = self.available_moves[last_col]
        assert(0 <= row <= self.board.get_row())
        # Update the board.
        self.board[row - 1, last_col] = self.__in_board_state['empty']
        self.player1_turn = not self.player1_turn
        self.available_moves[last_col] = row - 1
        
    
    def copy_board(self, other_board, player1_turn):
        '''
        Copy the state value from other board 'other_board' to 'self.board'.
        Argument:
        -- other_board: A list of lists in the following format:
           other_board[i][j] gives state at row 'i' and column 'j'.  
           Row index starts from bottom to top.
           Column index starts from left to right.
           Representation of state on board follows "self.__out_board_state".
           Use '|' to represent empty locations.  
           Use 'o' for player1 and 'x' for player2.
        -- player1_turn: Bool
        '''
        self.__check_valid_board(other_board, player1_turn)
        
        row = len(other_board)
        col = len(other_board[0])
        # Get a new instance of 'Board'.
        self.board = Board(row, col)
        for i in range(row):
            for j in range(col):
                if other_board[i][j] == self.__out_board_state["player1"]:
                    self.board[i, j] = self.__in_board_state["player1"]
                elif other_board[i][j] == self.__out_board_state["player2"]:
                    self.board[i, j] = self.__in_board_state["player2"]
                else:
                    self.board[i, j] = self.__in_board_state["empty"]
        self.player1_turn = player1_turn
        self.__find_available_moves()
                    
    # Helper functions:
    def __check_valid_board(self, board, player1_turn):
        '''
        Return True if 'board' is a valid ConnectFour board.
        '''
        # Test if 'board' is a list of lists of characters.
        if type(board) is not list:
            raise ConnectFourBoardError("Given board must be a list.")
        row = len(board)
        if type(board[0]) is not list:
            raise ConnectFourBoardError("First element of board is not a list") 
        col = len(board[0])
        # Check if 'board' is a list of lists of correct characters.
        num_1 = 0  # number of player1's discs
        num_2 = 0  # number of player2's discs
        for i in range(row):
            if type(board[i]) is not list:
                raise ConnectFourBoardError \
                      ("Given board must be a list of lists.")
            if len(board[i]) != col:
                raise ConnectFourBoardError \
                      ("The lists in the board must have the same length.")
            for j in range(col):
                if board[i][j] not in self.__out_board_state.values():
                    raise ConnectFourBoardError \
                          ("The state of the board must be from " + \
                           str(self.__out_board_state.values()))
                if board[i][j] == self.__out_board_state["player1"]:
                    num_1 += 1
                elif board[i][j] == self.__out_board_state["player2"]:
                    num_2 += 1
        # Check if the number of 'x's is 'almost' the same as 'o's.        
        if (num_1 == (num_2 + 1)) and player1_turn:
            raise ConnectFourBoardError("It should be player2's turn.")
        elif (num_2 == (num_1 + 1)) and (not player1_turn):
            raise ConnectFourBoardError("It should be player1's turn.")
        elif abs(num_1 - num_2) > 1:
            raise ConnectFourBoardError \
                  ("Invalid board: incorrect number of discs.")
        
        # Check if there are any empty locations below a disc on one column.
        for i in range(row - 1):
            for j in range(col):
                if board[i][j] == self.__out_board_state["empty"]:
                    # Check if the one location above it is empty.
                    if board[i + 1][j] != self.__out_board_state["empty"]:
                        raise ConnectFourBoardError \
                              ("Invalid board: empty location below a disc.")
        
    def __find_available_moves(self):
        '''
        Find next moves on the current board 'self.board'.
        '''
        # Find out the available next moves.  For each column, find out the top
        # position, which is the lowest row index with value of "empty".
        # Available next moves: a list of integers.  next_moves[i] is the next 
        # row index at column 'i'.
        col = self.board.get_col()
        row = self.board.get_row()
        next_moves = [0] * col
        # Indices of the columns that the program hasn't find the top positions.
        left_cols = range(col)
        # The idea for finding the lowest row index with value of "empty":
        # Go from bottom to top.  For each row, find the empty locations among 
        # all columns.  If a location is "empty", then the row index is stored
        # as the "next_moves" for this column.  
        # Because the location above an "empty" location is still "empty", so
        # once we found the 'next_moves' for a column, we should not consider
        # that column anymore.  This is why we have 'left_cols'.
        
        # TODO: other possible better ways to do this: search for each column
        # And probably we don't need to check afterwards.
        for i in range(row):
            for j in range(col):
                if (j in left_cols) and \
                   (self.board[i, j] == self.__in_board_state["empty"]):
                    next_moves[j] = i
                    left_cols.remove(j)
        assert(self.__check_available_moves(next_moves))
        self.available_moves = next_moves
        
    def __check_available_moves(self, moves):
        '''
        Return True if 'moves' is valid available moves on the current board.
        Argument:
        -- moves: A list of integers.
        Return: Bool
        '''
        row = self.get_row()
        col = self.get_col()
        for iCol in range(col):
            # For each column, all locations below moves[iCol] should be 
            # occupied by players and all rest should be empty.
            for iRow in range(moves[iCol]):
                if self.get_board_state(iRow, iCol) is \
                   self.__out_board_state["empty"]:
                    return False
            for iRow in range(moves[iCol], row):
                if self.get_board_state(iRow, iCol) is not \
                   self.__out_board_state["empty"]:
                    return False
        return True
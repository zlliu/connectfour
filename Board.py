'''
Implement the class 'Board'.
'''

class BoardError(Exception):
    pass

class IndexTypeError(BoardError):
    pass

class RowIndexError(BoardError):
    pass

class ColumnIndexError(BoardError):
    pass

class DiscError(BoardError):
    pass


class Board:
    '''
    This class represents a game board that could be used in Connect Four.
    Use '|' to represent empty locations, and use 'o' to represent disc1, and
    use 'x' to represent disc2.
    If given referencing index is of wrong type (not a tuple), the class raises
    IndexTypeError.
    If given row index is incorrect, the class raises RowIndexError;
    if given column index is incorrect, the class raises ColumnIndexError.
    If given board state is not from ('|', 'o', 'x'), the class raises DiscError
    All above three types of errors are belong to BoardError.
    
    Reasoning about this class:
    I want this class to be simply another composite data structure.  Currently,
    it seems 'stupid' because it's implementing a nested list data structure 
    using the built-in nested list.
    However, if in the future we want to change the implementation, eg. to a 
    list (not nested), then I only need to change codes in this class.
    So, I want everything else to be built on this abstraction layer.  That's
    why this class is very simple, it doesn't do anything else.  It doesn't try
    to implement a real ConnectFour board.  It's user's responsibility to 
    maintain a ConnectFour board.  Again, this class only gives a representation
    of the board.
    '''
    def __init__(self, row = 6, col = 7):
        '''
        Construct a board using a list of lists.
        '''        
        self.row = row
        self.col = col
        # A list of 'row' lists with length 'col'.  So, self.board[i][j] gives
        # state at row 'i' and column 'j'.  Row index starts from bottom to top.
        # Column index starts from left to right.  Both indices starts from 0.
        # Use '0' to represent empty locations.  Use '1' for player1 and '2' for
        # player2.
        self.board = []
        for i in range(self.row):
            self.board.append([0] * self.col)
        self.__board_state = {'disc1' : 'o', 'disc2' : 'x', 'empty': '|'}
        
    # Accessors:
    def get_col(self):
        '''
        Return the number of columns in the board.
        '''
        return self.col
    
    def get_row(self):
        '''
        Return the number of rows in the board.
        '''
        return self.row
    
    def __getitem__(self, index):
        '''
        Return the state of the board at row and column specified in 'index'.
        Operator overloading:
        self[row, col] is equivalent to self.__getitem__(row, col).
        (Note: 'row, col' creates a tuple (row, col).
        Argument:
        -- index: a tuple of two integers.
        Return:
        -- A character representing the state:
           'x': occupied by disc2
           'o': occupied by disc1
           '|': not occupied
        '''
        if type(index) is not tuple:
            raise IndexTypeError("Given index should be a tuple.")
        row, col = index
        return self._get_location(row, col)
    
    # Mutators:    
    def __setitem__(self, index, disc):
        '''
        Set the state of the board at row, and column given by 'index'.
        Operator overloading:
        self.__setitem__((row, col), disc) is equivalent to
        self[row, col] = disc.
        Argument:
        -- index: a tuple of two integers
        -- disc: a character from 'o', 'x', or '|'.
        '''
        if type(index) is not tuple:
            raise IndexTypeError("Given index should be a tuple.")
        row, col = index
        self._set_location(row, col, disc)
        
    # Helper functions.
    def _get_location(self, row, col):
        '''
        Return the state of the board at row 'row', and column 'col'.
        Argument:
        -- row: integer
        -- col: integer
        Return:
        -- A character representing the state:
           'x': occupied by disc2
           'o': occupied by disc1
           '|': not occupied
        '''
        if row not in range(self.row):
            raise RowIndexError("Incorrect row index!")
        if col not in range(self.col):
            raise ColumnIndexError("Incorrect column index!")
        board_value = self.board[row][col]
        if board_value == 1:
            return self.__board_state['disc1']
        if board_value == 2:
            return self.__board_state['disc2']
        return self.__board_state['empty']
    
    def _set_location(self, row, col, disc):
        '''
        Set the state of the board at row 'row', and column 'col'.
        Argument:
        -- row: integer
        -- col: integer
        -- disc: a character from 'o', 'x', or '|'.
        '''
        if row not in range(self.row):
            raise RowIndexError("Incorrect row index!")        
        if col not in range(self.col):
            raise ColumnIndexError("Incorrect column index!")
        if disc not in self.__board_state.values():
            raise DiscError("Incorrect disc.")
        if disc is self.__board_state['disc1']:
            self.board[row][col] = 1
        elif disc is self.__board_state['disc2']:
            self.board[row][col] = 2
        else:
            self.board[row][col] = 0
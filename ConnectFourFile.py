'''
Implement class "ConnectFourFile"
'''
from ConnectFour import *
class ConnectFourFileError(Exception):
    pass

class ConnectFourFile:
    '''
    Handles ConnectFour file operations.
    Depends on classes "ConnectFourBoard" and "ConnectFour".
    '''
    def __init__(self):
        '''
        Initialize internal state.
        '''
        # The state value from 'ConnectFourBoard' class.
        # Dictionary values are chosen based on how 'ConnectFourBoard'
        # represents board state, described in class docstring.
        self.__in_board_state = {'player1' : 'o', 'player2' : 'x', \
                                 'empty' : '|'}
        # Representation of board state in the saved file.
        self.__out_board_state = {'player1' : '1', 'player2' : '2', \
                                  'empty' : '0'}
        # values in following dictionaries are used to represent their keys
        # in the saved game.
        self.player_type_symbol = {"human" : 'h', "computer" : 'c'}
        self.player_turn_symbol = {"player1" : '1', "player2" : '2'}
        
    def save(self, game):
        '''
        Save the current game to a file with following format.
        1
        hc
        2211222
        0021221
        0021000
        0002000
        0001000
        0000000
        First line: 1 or 2 (1 if player1's turn; 2 if player2's turn.)
        Second line: hc or hh or ch (gives players types eg. hc means player1
                                     is human, while player2 is computer)
        Following lines: board configuration upside down
                         (0: empty; 1: occupied by player1;
                          2: occupied by player2)
        Argument:
        -- game: an instance of "ConnectFour"
        '''
        # Current program doesn't allow saving a finished game.
        print "Saving game..."
        filename = raw_input("Enter the file name: ")
        file_suffix = '.c4'
        myfile = open(filename + file_suffix, 'w')
        # 1st line: player's turn
        if game.get_current_player() is "player1":
            myfile.write(self.player_turn_symbol["player1"] + "\n")
        else:
            myfile.write(self.player_turn_symbol["player2"] + "\n")
        # 2nd line: players' type
        if game.get_player1_type() is "human":
            myfile.write(self.player_type_symbol["human"])
        else:
            myfile.write(self.player_type_symbol["computer"])
        if game.get_player2_type() is "human":
            myfile.write(self.player_type_symbol["human"] + "\n")
        else:
            myfile.write(self.player_type_symbol["computer"] + "\n")
        # Rest: board
        for i in range(game.get_row()):
            for j in range(game.get_col()):
                state = game.board.get_board_state(i, j)
                if state is self.__in_board_state["player1"]:
                    myfile.write(str(self.__out_board_state["player1"]))
                elif state is self.__in_board_state["player2"]:
                    myfile.write(str(self.__out_board_state["player2"]))
                else:
                    myfile.write(str(self.__out_board_state["empty"]))                  
            myfile.write("\n")
        myfile.close()    
        
    def load(self):
        '''
        Load a previously saved game.
        Return:
        -- game: an instance of 'ConnectFour'.
        '''
        # Loaded game is not finished because saving doesn't allow that.
        # So, no need to test if someone wins on a newly loaded game.
        print "loading game..."
        file_suffix = '.c4' # Avoid other files being opened.
        filename = raw_input("Enter the file name (.c4): ")                
        myfile = open(filename + file_suffix, 'r')
        # Load everything into temporary variables, then write to 'game'.
        
        # Load 1st line.
        first_line = myfile.readline()
        if len(first_line) != 2: # including "\n"
            raise ConnectFourFileError("First line has wrong length.")
        
        if first_line[0] == self.player_turn_symbol["player1"]:
            player1_turn = True
        elif first_line[0] == self.player_turn_symbol["player2"]:
            player1_turn = False
        else:
            raise ConnectFourFileError("First line is invalid.")
        # Load 2nd line.
        second_line = myfile.readline()
        if len(second_line) != 3: # including "\n"
            raise ConnectFourFileError("Second line has wrong length.")
        
        if second_line[0] == self.player_type_symbol["human"]:
            player1_is_human = True
        elif second_line[0] == self.player_type_symbol["computer"]:
            player1_is_human = False
        else:
            raise ConnectFourFileError \
                  ("Second line has invalid first character.")
        
        if second_line[1] == self.player_type_symbol["human"]:
            player2_is_human = True
        elif second_line[1] == self.player_type_symbol["computer"]:
            player2_is_human = False
        else:
            raise ConnectFourFileError \
                  ("Second line has invalid second character.")
        
        # Load rest lines: the board.
        board = []
        while True:
            line = myfile.readline()
            if line == '':
                break;
            
            current_row = []
            i = 0
            while line[i] != "\n":
                if line[i] is self.__out_board_state["player1"]:
                    current_row.append(self.__in_board_state["player1"])
                elif line[i] is self.__out_board_state["player2"]:
                    current_row.append(self.__in_board_state["player2"])
                elif line[i] is self.__out_board_state["empty"]:
                    current_row.append(self.__in_board_state["empty"])
                else:
                    raise ConnectFourFileError \
                          ("A line has invalid character.")
                # Go to next character.
                i += 1       
            board.append(current_row)
            
        # Check if board is a 'row' by 'col' matrix.
        row = len(board)
        if row == 0:
            raise ConnectFourFileError("Zero rows.")
        col = len(board[0])
        for i in range(row):
            if len(board[i]) != col:
                raise ConnectFourFileError \
                      ("Two rows have different columns.")
        myfile.close()        

        # Create a new game.
        game = ConnectFour(row, col)
        game.board.copy_board(board, player1_turn)
        if player1_is_human:
            game.set_player1_type("human")
        else:
            game.set_player1_type("computer")
        if player2_is_human:
            game.set_player2_type("human")
        else:
            game.set_player2_type("computer")
            
        return game

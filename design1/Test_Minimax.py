#
# Test script for minimax_connect4.py
#

import nose
import copy
from minimax_connect4 import *

#
# Data and files for testing.
#
row = 6
col = 7
total_four_connected_holes = (col - 3) * row + (row - 3) * col + \
    ((1 + 2 + 3) * 2) * 2
node1 = []
for i in range(row):
    node1.append([0] * col)
children1_1 = []
for i in range(col):
    temp = copy.deepcopy(node1)
    temp[0][i] = 1
    children1_1.append(temp)
children1_2 = []
for i in range(col):
    temp = copy.deepcopy(node1)
    temp[0][i] = -1
    children1_2.append(temp)
    
node2 = copy.deepcopy(node1)
node2[0][0] = -1
node2[0][1] = 1
node2[0][2] = 1
node2[1][2] = 1
node2[2][2] = 1
node2[0][3] = 1
node2[1][3] = -1
node2[2][3] = -1
node2[0][4] = -1
node2[1][4] = -1

children2 = []
for i in range(col):
    temp = copy.deepcopy(node2)
    if i in [0, 1]:
        temp[1][i] = 1
    elif i in [2, 3]:
        temp[3][i] = 1
    elif i == 4:
        temp[2][i] = 1
    else:
        temp[0][i] = 1
    children2.append(temp)

value_node1 = 0

value_node2 =  (-one) + (-two) + \
    (-one) + one + (three + two + one) + (-(one + two)) + (-(one + two))+ \
    (one + one) + (-one) + (-one) + \
    (-(two + two + one)) + two
    
#
# Helper functions.
#

def is_four_holes_connected(four_holes):
    '''
    Return True if the given four holes are connected.
    Argument:
    -- four_holes: A frozenset of 4 tuples, representing a location on the 
                   board (row, col).
    Return: bool.
    '''
    # Convert the frozenset to a list.
    four_holes_lst = list(four_holes)
    if len(four_holes_lst) != 4:
        return False
    first_hole = four_holes_lst[0]
    second_hole = four_holes_lst[1]
    if first_hole[0] == second_hole[0]:
        direction = "E-W"
    elif first_hole[1] == second_hole[1]:
        direction = "N-S"
    elif (first_hole[0] - second_hole[0]) == (first_hole[1] - second_hole[1]):
        direction = "NE-SW"
    elif (first_hole[0] - second_hole[0]) == -(first_hole[1] - second_hole[1]):
        direction = "NW-SE"
    else:
        return False
    
    for i in range(3):
        for j in range(i + 1, 4):
            one_hole = four_holes_lst[i]
            another_hole = four_holes_lst[j]
            if direction == "E-W":
                if not ((one_hole[0] == another_hole[0]) and \
                        ((one_hole[1] - another_hole[1]) in \
                         [1, 2, 3, -1, -2, -3])):
                    return False
            elif direction == "N-S":
                if not ((one_hole[1] == another_hole[1]) and \
                        ((one_hole[0] - another_hole[0]) in \
                         [1, 2, 3, -1, -2, -3])):
                    return False                
            elif direction == "NE-SW":
                if not (((one_hole[0] - another_hole[0]) == \
                         (one_hole[1] - another_hole[1])) and \
                        ((one_hole[1] - another_hole[1]) in \
                         [1, 2, 3, -1, -2, -3])):
                    return False                
            else:
                if not (((one_hole[0] - another_hole[0]) == \
                         (-(one_hole[1] - another_hole[1]))) and \
                        ((one_hole[1] - another_hole[1]) in \
                         [1, 2, 3, -1, -2, -3])):
                    return False
    # If all previous tests don't lead to False, then the given four holes
    # are indeed connected.  Proof as below:
    # First of all, there are four holes.
    # Second of all, all holes must be all in one of the four directions.
    # Thirdly, the biggest distance between two holes is 3.
    # Fourthly, there are at most 4 holes if the biggest distance is 3.
    # Fifthly, since it's converted from a set, all four holes are different.
    # Therefore, all four holes are connected.
    return True

def is_two_children_same(children1, children2):
    '''
    Return True if two list of children nodes are the same.
    Argument:
    -- children1: a list of nodes
    -- children2: a list of nodes
    return:
    -- bool
    '''
    temp1 = copy.deepcopy(children1)
    temp2 = copy.deepcopy(children2)
    # In order to compare if the elements in two lists are the same, we need
    # to sort the lists first.
    temp1.sort()
    temp2.sort()
    return temp1 == temp2

#
# Testing:
#

def test_generate_four_connected_holes():
    assert(len(four_connected_holes) == total_four_connected_holes)
    for ele in four_connected_holes:
        assert(is_four_holes_connected(ele))
            
def test_children():
    assert(is_two_children_same(children(node1, True), children1_1))
    assert(is_two_children_same(children(node1, False), children1_2))
    assert(is_two_children_same(children(node2, True), children2))
    
def test_heuristic_value():
    assert(heuristic_value(node1) == value_node1)
    assert(heuristic_value(node2) == value_node2)

def test_minimax():
    assert(minimax(node1, 0, True) == value_node1)
    assert(minimax(node2, 0, True) == value_node2)
    assert(minimax(node1, 0, False) == value_node1)
    assert(minimax(node2, 0, False) == value_node2)
    assert(minimax(node1, 1, True) == 7)
    assert(minimax(node2, 1, True) == inf)
    
    #assert(minimax(node1, 2, True) == 0)
    pass

def test_next_move():
    assert(next_move(node2, 1) == 2)
    temp = next_move(node1, 4)
    print temp
    assert(temp == 3)
    
if __name__ == '__main__':
    nose.runmodule()

'''
Utility functions implementing minimax algorithm for Connect Four game.
'''
import copy

inf = 10000000
# If four pieces are connected, return +inf for max, -inf for mini.
four = inf
# If three pieces are connected, 1000 for max, -1000 for mini.
three = 1000
# If two pieces are connected, 10 for max, -10 for mini.
two = 10
# If one piece.
one = 1
# Otherwise
zero = 0


# A global frozenset of frozensets of 4 tuples.  
# Each tuple represents a location (row, col) on the board.
# Each frozenset of 4 tuples represents four connected locations on the board.
# Initially a list, but will be converted to frozenset in 
# 'generate_four_connected_holes'.
four_connected_holes = []

# Reason for choosing frozensets.
# Use frozenset to store 'four_connected_holes'.
# 1st, Use list.  But when creating the list, make the element a frozenset.
# After creating the list, convert it to frozenset.
# The reason is you don't want to change them later on.  They are pre-computed.
# Tuples are immutable, so can be used as elements in a set.
# element of set has to be hashable, so lists and sets can't be the elements.
# But frozensets can be the elements.
# So, a set of frozensets
# If we have two sets of sets, then it's easy to compare.
# simply run A == B, then if A and B contain the same subsets, it returns True.
# Because in the set, the elements are not ordered, and are unique.
# So, no need to worry about the different order of when you write the set.

def generate_four_connected_holes(row = 6, col = 7):
    '''
    Generate all four connected holes.
    It modify the global list 'four_connected_holes'.  Each element of that list
    is a small list of four tuples, where each tuple gives the position on the
    board (row, col).
    '''
    global four_connected_holes
    # four in a row.
    for i in range(row):
        for j in range(col - 4 + 1):
            four_connected_holes.append(frozenset([(i, j), (i, j + 1), \
                                                   (i, j + 2), (i, j + 3)]))
    # four in a column.
    for i in range(col):
        for j in range(row - 4 + 1):
            four_connected_holes.append(frozenset([(j, i), (j + 1, i), \
                                                   (j + 2, i), (j + 3, i)]))
    
    # four in a diagonal: NE - SW
    for i in range(row - 4 + 1):
        for j in range(col - 4 + 1):
            four_connected_holes.append(frozenset([(i, j), \
                                                   (i + 1, j + 1), \
                                                   (i + 2, j + 2), \
                                                   (i + 3, j + 3)]))
    # four in a diagonal: NW - SE
    for i in range(row - 4 + 1, row):
        for j in range(col - 4 + 1):
            four_connected_holes.append(frozenset([(i, j), \
                                                   (i - 1, j + 1), \
                                                   (i - 2, j + 2), \
                                                   (i - 3, j + 3)]))
    four_connected_holes = frozenset(four_connected_holes)

# Run 'generate_four_connected_holes()' to pre-compute 'four_connected_holes'.
generate_four_connected_holes()

def next_move(current_node, depth = 1):
    '''
    Gives the next move based on the current board ('current_node').
    Argument:
    -- node: a list of lists, representing the current game progress.
             Use '0' to represent empty locations.  
             Use '1' for maximizing player ('max')
             Use '-1' for minimizing player ('mini')
    -- depth: integer, representing how many plies to go ahead.
    Return: 
    -- selected_col: An integer representing the column index.
    '''
    assert(is_node(current_node))
    assert(type(depth) is int)
    assert(depth >= 0)
    
    # The function assumes the current player is 'max'.
    max_turn = True
    available_moves = find_available_moves(current_node)
    maximum = -inf
    selected_col = -1
    
    for (i, child) in enumerate(children(current_node, max_turn)):
        # 'child' is the result of making 'available_moves[i]'.
        temp = minimax(child, depth - 1, not(max_turn))
        if maximum <= temp:
            maximum = temp
            selected_col = available_moves[i][1]
    assert(selected_col in range(len(current_node[0])))
    return selected_col

def minimax(node, depth, max_turn):
    '''
    Implement a position evaluation function based on minimax algorithm.
    Argument:
    -- node: a list of lists, representing the current game progress.
             Use '0' to represent empty locations.  
             Use '1' for maximizing player ('max')
             Use '-1' for minimizing player ('mini')
    -- depth: integer, representing how many plies to go ahead.
    -- max_turn: bool: True if it's max's turn.
    Return:
    -- integer, representing the value of current game progress.
    ----- The bigger the value, the better chance for max to win.
    ----- If max wins, the value is +inf; if mini wins, the value is -inf.
    '''
    # Assert on arguments.
    assert(is_node(node))
    assert(depth >= 0)
    assert(type(depth) is int)
    assert(type(max_turn) is bool)
    
    if (depth <= 0) or (is_terminal(node)):
        return heuristic_value(node)
    if max_turn:
        # max chooses the move with maximum value.
        value = -inf
        for child in children(node, max_turn):
            value = max(value, minimax(child, depth - 1, not(max_turn)))
        return value
    else:
        # mini chooses the move with the minimum value.
        value = inf
        for child in children(node, max_turn):
            value = min(value, minimax(child, depth - 1, not(max_turn)))
        return value

def is_terminal(node):
    '''
    If the current node is the terminal node, return True.
    '''
    # If the board is full, then it's a terminal node.
    if 0 not in node[-1]:  # node[-1] is the very top row.
        return True
    return False

def children(node, max_turn):
    '''
    Children nodes of the current node 'node'.
    Arguments:
    -- node: A list of lists, representing the game progress
    -- max_turn: True if its' max's turn.
    return:
    -- children: A list of lists of lists: a list of nodes.
    '''
    # Contract. (Test arguments)
    assert(is_node(node))
    assert(type(max_turn) is bool)    
    
    available_moves = find_available_moves(node)
    children = []
    if max_turn:
        for move in available_moves: # 'move' is a tuple.
            temp = copy.deepcopy(node)
            temp[move[0]][move[1]] = 1
            children.append(temp)
    else:
        for move in available_moves:
            temp = copy.deepcopy(node)    
            temp[move[0]][move[1]] = -1
            children.append(temp)
            
    return children

def find_available_moves(node):
    '''
    Find available moves on the current board.
    Argument:
    node-- A list of lists of tuples
    Return:
    available_moves-- A list of tuples
    '''
    # Test on Arguments.
    assert(is_node(node))
    row = len(node)
    col = len(node[0])
    # Find out the available next moves.  For each column, find out the top
    # position, which is the lowest row index with value of 0.
    available_moves = []  # Available next moves: a list of tuples (row, col).
    # Indices of the columns that the program hasn't find the top positions.
    left_cols = range(col)
    for i in range(row):
        for j in range(col):
            if (j in left_cols) and (node[i][j] == 0):
                available_moves.append((i, j))
                left_cols.remove(j)
    return available_moves

def heuristic_value(node):
    '''
    Return the heuristic value of the current game progress.
    '''
    assert(is_node(node))
    value = 0
    for ele in four_connected_holes:
        # 'ele' is a frozenset of 4 tuples.  
        # Each tuple gives the position (row, col)
        pegs_position = []
        for position in ele:
            pegs_position.append(node[position[0]][position[1]])
        value += pegs_value(pegs_position)
    # Make sure 'value' is within (-inf, inf).
    if value < -inf:
        value = -inf
    elif value > inf:
        value = inf
    return value

def pegs_value(pegs):
    '''
    Return the heuristic value of the pegs in any 4 connected holes.
    '''
    assert(type(pegs) is list)
    assert(len(pegs) == 4)
    
    num_pos = 0  # Number of +1s
    num_neg = 0  # Number of -1s
    for i in range(4):
        if pegs[i] == 1:
            num_pos += 1
        elif pegs[i] == -1:
            num_neg += 1
    if (num_pos == 4):
        return four
    if (num_pos == 3) and (num_neg == 0):
        return three
    if (num_pos == 2) and (num_neg == 0):
        return two
    if (num_pos == 1) and (num_neg == 0):
        return one
    if (num_neg == 4):
        return -four
    if (num_neg == 3) and (num_pos == 0):
        return -three
    if (num_neg == 2) and (num_pos == 0):
        return -two
    if (num_neg == 1) and (num_pos == 0):
        return -one
    return zero

def is_node(node):
    '''
    Return True if 'node' is a node representing a connect four board, i.e. 
    a list of lists of numbers, and numbers are either 0, 1, or -1.
    '''

    if type(node) is not list:
        return False
    row = len(node)    
    #if not len(node) == row:
     #   return False
    if row == 0:
        return False
    
    # row > 0
    if not type(node[0]) is list:
        return False  
    col = len(node[0])
    if col == 0:
        return False
    # col > 0
    for i in range(row):
        if type(node[i]) is not list:
            return False
        if not len(node[i]) == col:
            return False
        for j in range(col):
            if not node[i][j] in [1, -1, 0]:
                return False
    return True
'''
This program allows the user to interactively play the game of Connect Four.
'''

import sys
import random
import copy
from minimax_connect4 import *

class ConnectFourError(Exception):
    pass

class ConnectFourMoveError(ConnectFourError):
    pass

class ConnectFourCommandError(ConnectFourError):
    pass

class ConnectFourFileError(ConnectFourError):
    pass

class ConnectFour:
    '''
    Interactively play the game of ConnectFour.
    '''

    def __init__(self, row = 6, col = 7):
        '''
        Constructor: initialize internal state variables.
        '''
        self.row = row
        self.col = col
        # A list of 'row' lists with length 'col'.  So, self.game[i][j] gives
        # state at row 'i' and column 'j'.  Row index starts from bottom to top.
        # Column index starts from left to right.
        # Use '0' to represent empty locations.  Use '1' for player1 and '2' for
        # player2.
        self.game = []
        for i in range(self.row):
            self.game.append([0] * self.col)
        # All the moves made by two players (a list of tuples that give the
        # coordinates of the pegs each player have placed.)
        self.player1_moves = []
        self.player2_moves = []
        # 'available_moves[i]' gives next available location(index) at column i.
        self.available_moves = [0] * self.col
        self.player1_turn = True  # Player 1 always goes first.
        # A dictionary about which peg does each player use. (constant)
        # TODO: How to make a variable constant
        self.player_peg = {'player1' : 'o', 'player2' : 'x'} # 'o' goes first.
        # By default, player 1 is human, and player 2 is AI.
        self.player_type = {'player1_is_human' : True, 
                            'player2_is_human' : False}

    def find_next_moves(self):
        '''
        Find next moves on the current board.
        '''

        # Find out the available next moves.  For each column, find out the top
        # position, which is the lowest row index with value of 0.
        # Available next moves: a list of integers.  next_moves[i] is the next 
        # row index at column 'i'.
        next_moves = [0] * self.col
        # Indices of the columns that the program hasn't find the top positions.
        left_cols = range(self.col)
        for i in range(self.row):
            for j in range(self.col):
                if (j in left_cols) and (self.game[i][j] == 0):
                    next_moves[j] = i
                    left_cols.remove(j)
        # TODO: Check if available_moves is correct.
        self.available_moves = next_moves
    
    def is_valid_board(self, board):
        '''
        Check if the given game configuration 'board' is valid.
        '''
        # TODO.
        return True

    def load(self):
        '''
        Load a previously saved game.
        '''
        print "loading game..."
        file_suffix = '.c4' # Avoid other files being opened.
        while True:
            try:
                filename = raw_input("Enter the file name (.c4): ")                
                myfile = open(filename + file_suffix, 'r')
                # Load everything into temporary variables.
                # Don't change the data members until the end, when no file
                # format errors happen.
                first_line = myfile.readline()
                if first_line == '1\n':
                    player1_turn = True
                elif first_line == '2\n':
                    player1_turn = False
                else:
                    raise ConnectFourFileError("First line is invalid.")
                
                second_line = myfile.readline()
                if len(second_line) != 3: # including "\n"
                    raise ConnectFourFileError("Second line has invalid length.")
                    
                if second_line[0] == 'h':
                    player1_is_human = True
                elif second_line[0] == 'c':
                    player1_is_human = False
                else:
                    raise ConnectFourFileError \
                          ("Second line has invalid first character.")

                if second_line[1] == 'h':
                    player2_is_human = True
                elif second_line[1] == 'c':
                    player2_is_human = False
                else:
                    raise ConnectFourFileError \
                          ("Second line has invalid second character.")
                
                board = []
                while True:
                    line = myfile.readline()
                    if line == '':
                        break;
                    if len(line) != (self.col + 1): # including "\n"
                        raise ConnectFourFileError \
                              ("A line has invalid length.")                              

                    for char in line:
                        if not char in ['0', '1', '2', '\n']:
                            raise ConnectFourFileError \
                                  ("A line has invalid character.")
                                  
                            
                    current_row = []
                    for i in range(self.col):
                        current_row.append(int(line[i]))
                    board.append(current_row)
                    
                
                if len(board) != self.row:
                    raise ConnectFourFileError \
                          ("Wrong number of rows.")
                
                if not self.is_valid_board(board):
                    raise ConnectFourFileError \
                          ("Not a valid game board.")
                    
                myfile.close()
                break
            except IOError:
                print "File " + filename + file_suffix + " doesn't exist."
                print "Please give a valid filename."
            except ConnectFourFileError, e:
                print "Invalid file format."
                print e
                print "Please give another valid file (.c4)."

        # Copy temporary variables to data members.
        # TODO: should make loading be able to load a game with different row
        # and column.
        self.game = copy.deepcopy(board)
        self.player1_turn = player1_turn
        self.player_type["player1_is_human"] = player1_is_human
        self.player_type["player2_is_human"] = player2_is_human
        self.player1_moves = []
        self.player2_moves = []                
        self.find_next_moves()
        # Continue game from here.
        self.play_game()

    def save(self):
        '''
        Save the current game to a file with following format.
        1
        hc
        2211222
        0021221
        0021000
        0002000
        0001000
        0000000
        First line: 1 or 2 (1 if player1's turn; 2 if player2's turn.)
        Second line: hc or hh or ch (gives players types eg. hc means player1
                                     is human, while player2 is computer)
        Following lines: board configuration upside down
                         (0: empty; 1: occupied by player1;
                          2: occupied by player2)
        '''
        print "Saving game..."
        filename = raw_input("Enter the file name: ")
        file_suffix = '.c4'
        myfile = open(filename + file_suffix, 'w')
        if self.player1_turn:
            myfile.write("1\n")
        else:
            myfile.write("2\n")
        if self.player_type["player1_is_human"]:
            myfile.write('h')
        else:
            myfile.write('c')
        if self.player_type["player2_is_human"]:
            myfile.write("h\n")
        else:
            myfile.write("c\n")
        for i in range(self.row):
            for j in range(self.col):
                myfile.write(str(self.game[i][j]))
            myfile.write("\n")
        myfile.close()
        
        

    def show(self):
        '''
        Pretty-print the current game.
        '''
        print
        print "=================="        
        print "   Connect Four"
        # Print header.
        sys.stdout.write('   ')
        for i in range(self.col):
            sys.stdout.write('%d ' % i)
        print
        # Print game.
        for i in range(self.row - 1, -1, -1):
            sys.stdout.write(' %d ' % i)
            for j in range(self.col):
                if self.game[i][j] == 0:
                    sys.stdout.write('| ')
                elif self.game[i][j] == 1:
                    sys.stdout.write('%s ' % self.player_peg['player1'])
                elif self.game[i][j] == 2:
                    sys.stdout.write('%s ' % self.player_peg['player2'])
                else:
                    raise ConnectFourError('Incorrect game configuration.')
            print
        if self.player_type['player1_is_human']:
            player1_type_str = 'human'
        else:
            player1_type_str = 'computer'
        if self.player_type['player2_is_human']:
            player2_type_str = 'human'
        else:
            player2_type_str = 'computer'
        print "player1 (%s): %s" % \
              (self.player_peg['player1'], player1_type_str)
        print "player2 (%s): %s" % \
              (self.player_peg['player2'], player2_type_str)
        print "=================="                
        print
       
    def user_input(self):
        '''
        Ask user for proper input.
        '''
        while True:
            print 
            print 'Enter the column number: 0 - ' + str(self.col - 1)
            print 'Or use following commands:'
            print 's: save current game.'
            print 'l: load previous saved game.'
            print 'h: give a hint.'
            print 'e: exit game.'
            try:
                user_cmd = raw_input('Please make your move: ')
                if user_cmd == 's':
                    self.save()
                elif user_cmd == 'l':
                    self.load()
                elif user_cmd == 'h':
                    return self.hint()
                elif user_cmd == 'e':
                    sys.exit(0)  # Question: Is it a good way?
                else:
                    selected_col = int(user_cmd)
                    if selected_col not in range(self.col):
                        raise ConnectFourMoveError \
                              ('Error: Input is not in range.')
                    if self.available_moves[selected_col] >= self.row:
                        raise ConnectFourMoveError \
                              ('Error: No pegs can be put on selected column!')
                    return selected_col
            except ValueError:
                print 'Error: Invalid input!'
            except ConnectFourMoveError, e:
                print e
            self.show()
    
    def select_col(self, level = 0):
        '''
        Selects a column number based on minimax algorithm.
        '''
        if level == 0:
            # Implement stupid computer by randomly selecting columns.
            random.seed()
            while True:
                selected_col = random.randint(0, self.col - 1)
                if self.available_moves[selected_col] < self.row:
                    return selected_col
        else:
            node = self.convert_board()
            return next_move(node, level)
    
    def convert_board(self):
        '''
        Convert current board configuration 'self.game' into a 'node' that 
        minimax algorithm in module 'minimax_connect4' works on.
        'node':
        Use '0' to represent empty locations.  
        Use '1' for maximizing player ('max')
        Use '-1' for minimizing player ('mini')
        It decides current current player to be 'max'.
        Argument: none
        Return: 
        node -- A list of lists of numbers.
        '''
        # Check whose turn it is.
        if self.player1_turn:
            # player1 is the maximizing player.
            node = copy.deepcopy(self.game)
            for i in range(self.row):
                for j in range(self.col):
                    if node[i][j] == 2: # 2 means player2 in 'self.game'
                        # player1 is minimizing player
                        node[i][j] = -1
        else:
            # player2 is the maximizing player.
            node = copy.deepcopy(self.game)
            for i in range(self.row):
                for j in range(self.col):
                    if node[i][j] == 1: # 1 means player1 in 'self.game'
                        # player1 is minimizing player
                        node[i][j] = -1
                    elif node[i][j] == 2: # 2 means player2 in 'self.game'
                        node[i][j] = 1        
        return node
        
    def update_move(self, peg_col):
        '''
        Update the move.  (Not used right now)
        '''
        peg_row = self.available_moves[peg_col]
        # Find out whose turn it is.
        if self.player1_turn:
            self.player1_moves.append((peg_row, peg_col))
            self.game[peg_row][peg_col] = 1
            self.player1_turn = False
        else:
            self.player2_moves.append((peg_row, peg_col))
            self.game[peg_row][peg_col] = 2
            self.player1_turn = True
        self.available_moves[peg_col] = peg_row + 1
    
    def someone_wins(self, moves):
        '''
        Return True if current player wins with current 'moves'.
        'moves' is a list of tuples (peg_row, peg_col)
        '''
        # Only need to check if the newest added peg results in four pegs in
        # a row, column, or diagonal.
        if len(moves) == 0:
            return False
        new_peg = moves[-1]
        # Four directions at 'new_peg'.
        # (0, 1) gives the horizontal direction, because neighboring locations
        # are generated by adding to or subtracting from 'new_peg' by (0, 1).
        # Similarly, (1, 0) is for vertical direction; (1, 1) is for 45-degree
        # diagonal; (-1, 1) is for 135-degree diagonal.
        direction = [(0, 1), (1, 0), (1, 1), (-1, 1)]
        for ele in direction:
            if self.four_pegs_connected(new_peg, ele, moves):
                return True
        return False
         
    def four_pegs_connected(self, starting_loc, direction, moves):
        '''
        Return True if there are four pegs including
        the peg on 'starting_loc' on the given line, 
        'starting_loc' is a tuple, representing a location on the board.
        'direction' is a tuple.  Below is how to interpret it.
        (0, 1) gives the horizontal direction, because neighboring locations
        are generated by adding to or subtracting from 'starting_loc' by (0, 1).
        Similarly, (1, 0) is for vertical direction; (1, 1) is for 45-degree
        diagonal; (-1, 1) is for 135-degree diagonal.
        '''
        connected_pegs = [starting_loc] # A list of tuples.        
        # The two locations at both ends of the current connected pegs.
        one_end = (starting_loc[0] + direction[0], \
                   starting_loc[1] + direction[1])
        other_end = (starting_loc[0] - direction[0], \
                     starting_loc[1] - direction[1])
        while True:
            if 0 <= one_end[0] < self.row and \
               0 <= one_end[1] < self.col and \
               one_end in moves: 
                # One end is occupied by a peg.
                connected_pegs.append(one_end)
                if len(connected_pegs) == 4:
                    return True
                one_end = (one_end[0] + direction[0], \
                           one_end[1] + direction[1])
                continue
            elif 0 <= other_end[0] < self.row and \
               0 <= other_end[1] < self.col and \
               other_end in moves: 
                # The other end is occupied by a peg.
                connected_pegs.append(other_end)
                if len(connected_pegs) == 4:
                    return True
                other_end = (other_end[0] - direction[0], \
                             other_end[1] - direction[1])
                continue
            else:
                return False
            
    def undo(self):
        '''
        Undo the current move.
        '''
        # TODO
        pass
    
    def hint(self):
        '''
        Give a hint for next move.
        Return:
        -- selected_col: A integer
        '''
        # TODO
        pass
    
    def play_one_game(self):
        '''
        Play the game for one time.
        '''
        # Display the initial game.
        self.show()
        while True:
            # Ask for inputs.
            if self.player1_turn:
                if self.player_type['player1_is_human']:
                    print "Player1's turn:"
                    peg_col = self.user_input()
                else:
                    peg_col = self.select_col(2)
                # Update game states:
                peg_row = self.available_moves[peg_col]
                self.player1_moves.append((peg_row, peg_col))
                self.game[peg_row][peg_col] = 1
                self.available_moves[peg_col] = peg_row + 1
                # Display:
                self.show()
                # Check if player1 wins.
                if self.someone_wins(self.player1_moves):
                    print "Congratulations, Player1!  You win!"
                    break
                # Switch to player2.
                self.player1_turn = False                          
            else:
                if self.player_type['player2_is_human']:
                    print "Player2's turn:"
                    peg_col = self.user_input()
                else:
                    peg_col = self.select_col(2)
                # Update game states:
                peg_row = self.available_moves[peg_col]
                self.player2_moves.append((peg_row, peg_col))
                self.game[peg_row][peg_col] = 2
                self.available_moves[peg_col] = peg_row + 1
                # Display:
                self.show()              
                # Check if player2 wins.
                if self.someone_wins(self.player2_moves):
                    print "Congratulations, Player2!  You win!"
                    break                  
                # Switch to player1.
                self.player1_turn = True 

    def play_game(self):
        '''
        Play game continuously.
        '''
        # Play game.
        play_game = True
        while play_game:
            self.play_one_game()
            while True:
                user_input = raw_input \
                    ("Do you want to play one more game? (y/n): ")
                if user_input == 'y':
                    play_game = True
                    last_player1_type = self.player_type['player1_is_human']
                    last_player2_type = self.player_type['player2_is_human']
                    # Initialize a new game.
                    self.__init__()
                    # TODO: Try setting players' types when calling '__init__'
                    # with default arguments.
                    if self.game_mode == '1':
                        # Switch the order of first moves if playing against AI.
                        self.player_type['player1_is_human'] = not \
                            last_player1_type
                        self.player_type['player2_is_human'] = not \
                            last_player2_type
                    else:
                        self.player_type['player1_is_human'] = last_player1_type
                        self.player_type['player2_is_human'] = last_player2_type
                    break
                if user_input == 'n':
                    play_game = False
                    break
                print "Invalid input!"
                
    def run(self):
        '''
        Play Connect Four games.
        '''
        # Welcoming message.
        print 'Play Connect 4!'
        
        # Select game mode.
        while True:
            print 'Game mode:'
            print '1. Against a computer'
            print '2. Between two human'            
            self.game_mode = raw_input("Select game mode (1/2): ")
            if self.game_mode == '1': # default choice
                break
            if self.game_mode == '2':
                self.player_type['player1_is_human'] = True
                self.player_type['player2_is_human'] = True
                break
            print 'Invalid input!'
        self.play_game()
        

        
            
        # Advanced features:
        # 1.Ask user what kind of game to play. (7x6 or 8x7 etc..)
        # 2.Ask user to select difficulty levels.
        # 3.Add command: give up.
        # 4.Add info about history records.
        # 5.Make something easier for human than level 2
        
if __name__ == '__main__':
    s = ConnectFour()
    s.run()


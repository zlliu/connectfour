'''
This module implements 'Minimax' class.
'''
import copy
from ConnectFourBoardEvaluator import *
from MinimaxNode import *

class Minimax:
    '''
    Implements Minimax algorithm for Connect Four game.
    '''
    
    def __init__(self, row = 6, col = 7):
        '''
        Constructor.
        '''
        self.row = row
        self.col = col
        self.evaluator = ConnectFourBoardEvaluator(row, col)
    
    def next_move(self, board_node, depth = 1):
        '''
        Gives the next move for maximizing player on the current board 
        ('board_node') based on "Minimax" algorithm.
        maximizing player's discs are represented as 1 in 'board_node'.
        Argument:
        -- board_node: a board representation with the same format as in 
                       self.node of "MinimaxNode".
        -- depth: integer, representing how many plies to go ahead.
        Return: 
        -- selected_col: An integer representing the column index.
        '''
        assert(type(depth) is int)
        assert(depth >= 0)
        
        # The function assumes the current player is maximizing player ('max').
        max_turn = True
        maximum = -self.evaluator.get_inf()
        selected_col = -1
        children = self.__children_nodes(MinimaxNode(board_node), max_turn)
        for move in children:
            child = children[move]
            # 'child' is the result of making 'move'.
            #self.minimax(child, depth - 1, not(max_turn))
            self.alphabeta(child, -self.evaluator.get_inf(), \
                           self.evaluator.get_inf(), depth - 1, not(max_turn))
            this_value = child.get_value()
            if maximum <= this_value:
                maximum = this_value
                selected_col = move[1] # only need the column.
        return selected_col
    
    # Helpers.
    def minimax(self, minimax_node, depth, max_turn):
        '''
        Implement a position evaluation function based on minimax algorithm.
        It sets a board value to 'minimax_node'.
        The board value is an integer:
        ----- The bigger the value, the better chance for max to win.
        ----- If max wins, the value is +inf; if mini wins, the value is -inf.
        Argument:
        -- minimax_node: reference to an instance of class 'MinimaxNode'.
        -- depth: integer, representing how many plies to go ahead.
        -- max_turn: bool: True if it's max's turn.
        Return: None
        '''
        # Assert on arguments.
        assert(depth >= 0)
        assert(type(depth) is int)
        assert(type(max_turn) is bool)
        if (depth <= 0) or (self.is_terminal(minimax_node)):
            minimax_node.set_value(self.evaluator.evaluate(minimax_node))
            return
        # TODO: following codes can by made 'DRYER'.
        children = self.__children_nodes(minimax_node, max_turn)
        if max_turn:
            # max chooses the move with maximum value.
            value = -self.evaluator.get_inf()
            for move in children:
                child = children[move]
                self.minimax(child, depth - 1, not(max_turn))
                value = max(value, child.get_value())
        else:
            # mini chooses the move with the minimum value.
            value = self.evaluator.get_inf()
            for move in children:
                child = children[move]
                self.minimax(child, depth - 1, not(max_turn))                
                value = min(value, child.get_value())
        # Since "minimax_node" is a reference, we can change it outside by 
        # change it inside this function.
        # Note if a function still returns None if there is no return keyword.
        # Also note that min(10, None) returns None
        minimax_node.set_value(value)
        return  # not necessary because it's the end of the function.

    def alphabeta(self, minimax_node, alpha, beta, depth, max_turn):
        '''
        Implement a position evaluation function based on minimax algorithm with
        alpha-beta pruning.
        It sets a board value to 'minimax_node'.
        The board value is an integer:
        ----- The bigger the value, the better chance for max to win.
        ----- If max wins, the value is +inf; if mini wins, the value is -inf.
        Argument:
        -- minimax_node: reference to an instance of class 'MinimaxNode'.
        -- alpha: number, the maximum lower bound of possible solutions
        -- beta: number, the minimum upper bound of possible solutions
        -- depth: integer, representing how many plies to go ahead.
        -- max_turn: bool: True if it's max's turn.
        Return: None
        '''
        # Assert on arguments.
        assert(self.__is_number(alpha))
        assert(self.__is_number(beta))
        assert(alpha < beta)
        assert(type(depth) is int)
        assert(depth >= 0)        
        assert(type(max_turn) is bool)
        
        if (depth == 0) or (self.is_terminal(minimax_node)):
            minimax_node.set_value(self.evaluator.evaluate(minimax_node))
            return
        # TODO: following codes can by made 'DRYER'.
        children = self.__children_nodes(minimax_node, max_turn)
        if max_turn:
            # "max" chooses the move with maximum value.
            value = -self.evaluator.get_inf()
            for move in children:
                child = children[move]
                self.alphabeta(child, alpha, beta, depth - 1, not(max_turn))
                # Because this node is 'max', 'max' chooses the node with
                # the maximum value. 
                value = max(value, child.get_value())
                # alpha is the maximum lower bound.  
                # So, alpha has to be no smaller than values of all children.
                alpha = max(alpha, child.get_value())
                if alpha >= beta:
                    # The sub-tree starting at this node will not contain
                    # the lucky one.
                    # beta pruning.
                    # At the beginning of this function, beta >  alpha.
                    # Now alpha >= beta, so alpha has been changed.
                    # So, value must have been changed to the same as alpha now.
                    break
        else:
            # mini chooses the move with the minimum value.
            value = self.evaluator.get_inf()
            for move in children:
                child = children[move]
                self.alphabeta(child, alpha, beta, depth - 1, not(max_turn))
                value = min(value, child.get_value())
                beta = min(beta, child.get_value())
                if alpha >= beta:
                    # alpha pruning.
                    break
        # Since "minimax_node" is a reference, we can change it outside by 
        # change it inside this function.
        # Note if a function still returns None if there is no return keyword.
        # Also note that min(10, None) returns None
        minimax_node.set_value(value)
        return  # not necessary because it's the end of the function.
    
    def __children_nodes(self, minimax_node, max_turn):
        '''
        Find children MinimaxNodes of 'minimax_node'.  More specifically,
        generate all the possible boards after one ply from the current board, 
        based on whose turn it is.
        Argument:
        -- minimax_node: an instance of class "MinimaxNode".
        -- max_turn: bool, True if it's max's turn, and False if not.
        Return:
        -- children: a dictionary, whose keys are available moves, and whose 
                     values are instances of 'MinimaxNode', representing the 
                     child node after making a move (its key).
        '''
        # Contract. (Test arguments)
        assert(type(max_turn) is bool)
        children = {}
        available_moves = self.__find_available_moves(minimax_node)
        for move in available_moves: # 'move' is a tuple.
            temp = copy.deepcopy(minimax_node.get_node())
            if max_turn:
                temp[move[0]][move[1]] = 1
            else:
                temp[move[0]][move[1]] = -1                    
            children[move] = MinimaxNode(temp)
        return children
    
    def __find_available_moves(self, minimax_node):
        '''
        Find available moves on the current board.
        Argument:
        -- minimax_node: an instance of class "MinimaxNode".
        Return:
        -- available_moves: A list of tuples, representing a location (row, col)
        '''
        # Find out the available next moves.  For each column, find out the top
        # position, which is the lowest row index with value of 0.
        available_moves = []
        # Indices of the columns that the program hasn't find the top positions.
        left_cols = range(self.col)
        board_node = minimax_node.get_node()  # A reference to the board node.
        for i in range(self.row):
            for j in range(self.col):
                if (j in left_cols) and (board_node[i][j] == 0):
                    available_moves.append((i, j))
                    left_cols.remove(j)
        return available_moves   
     
    def is_terminal(self, minimax_node):
        '''
        If the current minimax node is the terminal node, return True.
        '''
        # If the board is full, then it's a terminal node.
        if 0 not in minimax_node.get_node()[-1]:
            # minimax_node[-1] is the very top row.
            return True
        return False
    
    def __is_number(self, arg):
        '''
        Return True if 'arg' is an number (integer or float).
        '''
        if (type(arg) is int) or (type(arg) is float):
            return True
        return False
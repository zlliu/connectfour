Project Title: Connect Four
Description:
In this project, I will implement the game Connect Four 
(http://en.wikipedia.org/wiki/Connect_Four).
The algorithm I plan to use is the minimax algorithm.
My program should have a user interface in the terminal that allows two players
to compete each other or a player to play with a computer.  
The computer should be intelligent to beat people.
Ideally, user can select different levels of computer to play with.

Programming Languages: Python, C
External Libraries: Up to this point, I don't know if I need external libraries.
Risk Analysis:
1. The algorithm part would be challenging.  I don't know about the minimax 
algorithm, so I need to learn the algorithm, and apply it to our problem.
2. How to manage a project is also challenging for me, because I never developed
a whole project from the beginning.  So, I need to learn how to decompose a 
whole project into different modules, and decompose a problem into different
functions.  And I need to practice debugging, which I had little experience with
before.
3. Implementing the project in C is challenging, too.
4. How to build graphic user interface in python or C is new to me.
5. If necessary, I may need to learn interface between C and Python.  If Python 
has a much better user interface library.  I need to learn how to call a module
 written in C.
6. Object oriented programming is hard for me, too.  I am still not used to 
think of programming that way.  I will force myself to consider using classes 
whenever appropriate.

Implementation Plan:
I will use the source control software git.
Week 2-3: Implement a simple user interface in the terminal.  So, people can play
with people, and the computer can make random moves to play with people.  Test it.
Week 4: Learn minimax algorithm, and design an algorithm for my program.
Week 5-6: Implement the algorithm that enable a computer to make intelligent 
moves. (I will call this algorithm 'decision algorithm')   Test it.
Week 7-8: Implement the core part (the decision algorithm) in C.
Week 9-10: Implement a graphical user interface in Python.  And implement an 
interface between C and Python, so that the Python GUI can call the decision 
algorithm written in C. 
If time left: I will try to make my game more playable by providing different 
difficulty levels, which means the decision algorithm should have a variable 
controlling how deep (how many plies) to go.  Also, my game can not only provide
the standard Connect Four (7X6), but also 8X7, 9X7, and 10X7.  So, my user 
interface and decision algorithm should provide interfaces to these parameters.
Source Code Availability:
Use source control software git.  Use bitbucket.org as the remote repository.
To get a copy of the repo (public repo):
git clone https://zlliu@bitbucket.org/zlliu/connectfour.git

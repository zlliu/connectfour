'''
Test for ConnectFourBoard class.
'''

import nose
import copy
from ConnectFourBoard import *

#
# Data for testing.
#
b1 = []
for i in range(6):
    b1.append(['|'] * 7)
b2 = copy.deepcopy(b1)
b2[0][0] = 'o'
b2[0][1] = 'x'
b3 = copy.deepcopy(b2)
b3[1][0] = 'o'
b4 = copy.deepcopy(b1)
b4[1][1] = 'x'

#
# Test.
#
def test_ConnectFourBoard():
    # Test on 'get_row' and 'get_col'.
    board1 = ConnectFourBoard()
    assert(board1.get_row() == 6)
    assert(board1.get_col() == 7)
    board2 = ConnectFourBoard(7, 8)
    assert(board2.get_row() == 7)
    assert(board2.get_col() == 8)
    
    # Test on 'get_board_state' etc.
    assert(board1.get_board_state(3, 4) == '|')
    assert(board1.get_available_columns() == range(7))
    assert(board1.get_current_player() == 'player1')
    
    board1.add_to_column(0)
    assert(board1.get_board_state(0, 0) == 'o')
    assert(board1.get_available_columns() == range(7))
    assert(board1.get_current_player() == 'player2')
    # Test on 'get_available_columns'
    for i in range(5):
        board1.add_to_column(0)
    assert(board1.get_board_state(5, 0) == 'x')        
    assert(board1.get_available_columns() == range(1, 7))
    assert(board1.get_current_player() == 'player1')
    # Test on 'remove_last_disc'
    board1.remove_last_disc()
    assert(board1.get_board_state(5, 0) == '|')        
    assert(board1.get_available_columns() == range(7))
    assert(board1.get_current_player() == 'player2')
    
    # Test on 'copy_board'
    board1.copy_board(b1, True)
    assert(board1.get_board_state(0, 0) == '|')        
    assert(board1.get_available_columns() == range(7))
    assert(board1.get_current_player() == 'player1')
    board1.copy_board(b2, True)
    assert(board1.get_board_state(0, 0) == 'o')   
    assert(board1.get_board_state(0, 1) == 'x')        
    assert(board1.get_board_state(0, 2) == '|')            
    assert(board1.get_available_columns() == range(7))
    assert(board1.get_current_player() == 'player1')
    board1.copy_board(b3, False)
    assert(board1.get_board_state(0, 0) == 'o')   
    assert(board1.get_board_state(0, 1) == 'x')        
    assert(board1.get_board_state(1, 0) == 'o')  
    assert(board1.get_available_columns() == range(7))
    assert(board1.get_current_player() == 'player2')
    try:
        board1.copy_board(b4, False)
        assert(False)
    except ConnectFourBoardError:
        assert(True)
    try:
        board1.copy_board(b3, True)
        assert(False)
    except ConnectFourBoardError:
        assert(True)
    
if __name__ == "__main__":
    nose.runmodule()

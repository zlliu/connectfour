'''
Test for Board class.
'''

import nose
from Board import *

#
# Tests.
#
def test_Board():
    board1 = Board()
    assert(board1.get_row() == 6)
    assert(board1.get_col() == 7)
    
    assert(board1[4, 5] == '|')  # Default is empty.    
    board1[0, 0] = 'x'
    assert(board1[0, 0] == 'x')
    board1[3, 4] = 'o'
    assert(board1[3, 4] == 'o')
    board1[3, 4] = '|'
    assert(board1[3, 4] == '|')
    
    # Test on exceptions.
    try:
        board1[1, 2] = '*'
        assert(False)
    except BoardError, e:
        print e
        assert(True)
        
    try:
        board1[2, 10] = 'x'
        assert(False)
    except ColumnIndexError, e:
        print e
        assert(True)
    
    try:
        board1[10, 1] = 'x'
        assert(False)
    except Exception, e:
        print e
        assert(True)    
    
    board2 = Board(7, 8)
    assert(board2.get_row() == 7)
    assert(board2.get_col() == 8)
    
if __name__ == "__main__":
    nose.runmodule()
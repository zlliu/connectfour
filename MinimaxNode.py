'''
Implement class 'MinimaxNode'.
'''

class MinimaxNode:
    '''
    This class contains a representation of a Connect Four board called 'node', 
    and a value associated with it.
    'node' is a list of lists.  
    node[i][j] gives state at row 'i' and column 'j'. 
    Row index starts from bottom to top, and column index starts from left to 
    right.
    'node[i][j]':
    Use '0' to represent empty locations.  
    Use '1' for maximizing player ('max')
    Use '-1' for minimizing player ('mini')
    The board value is an integer:
    ----- The bigger the value, the better chance for max to win.
    ----- If max wins, the value is +inf; if mini wins, the value is -inf.
    
    Reasoning for this class:
    This class is more like a struct.  It combine the node with its value.
    Another benefit: No need to check if the 'node' in the Minimax algorithm is
    a valid node.
    As long as the Minimax algorithm uses an instance of this class, it 
    guarentee that 'node' is valid, because it checks every time an instance is
    created.  It's like an automatic warranty.
    '''
    def __init__(self, node):
        '''
        Construct a new Minimax node.
        '''
        # Use assertion instead of exception because the only place a new
        # MinimaxNode instance is created is inside class "Minimax".
        # It's other codes' responsibility to make sure 'node' is valid,
        # not the user's responsibility, and so user don't expect to see
        # the error message from here.
        # So, if algorithm is correct, "node" should always be valid.
        assert(self._is_node(node))
        # TODO: change data type of 'node' to tuple of tuples, which is 
        # immutable, and can be accessed the same as list.
        # The board 'node' should not be changed.
        self.node = node
        # Default number of 'self.value' is 0.
        # It's meaningless if 'self.__is_evaluated' is False
        self.value = 0
        self.__is_evaluated = False
        # Depth below this node.  So the leaf node has depth of 0.
        self.depth = -1
        ## Use alpha-beta pruning.
        ## alpha: the maximum lower bound of possible solutions
        ## beta: the minimum upper bound of possible solutions
        #self.alpha = 0
        #self.beta = 0
        #self.max_turn = True
        ## Parent node:
        #self.parent = None
        
    # Accessors.
    def get_node(self):
        '''
        Return the node.
        '''
        return self.node
    
    def get_value(self):
        '''
        Return the value of this node.
        '''
        # Use assertion instead of exception because correct algorithm should
        # make sure no error occurs here.
        assert(self.__is_evaluated)
        return self.value
    
    #def get_depth(self):
        #'''
        #Return the depth below this node.
        #'''
        ## self.depth has to be assigned first.
        #assert(self.depth != -1)
        #return self.depth
    
    #def get_alpha(self):
        #'''
        #Return the alpha value of this node.
        #'''
        #return self.alpha
    
    #def get_beta(self):
        #'''
        #Return the beta value of this node.
        #'''
        #return self.beta
    
    #def is_max_turn(self):
        #'''
        #Return True if it's "max" turn on this node.
        #'''
        #return self.max_turn
    
    # Mutators.
    def set_value(self, node_value):
        '''
        Set the value of this node.
        '''
        # Assertion is important.  With following assertion, a bug will shows
        # up more easily.
        # Remember, "GIGO": garbage in, garbage out.
        assert(type(node_value) is int or type(node_value) is float)
        self.value = node_value
        self.__is_evaluated = True
        
    #def set_depth(self, depth):
        #'''
        #Set the depth of this node.
        #'''
        #assert(type(depth) is int)
        #assert(depth >= 0)
        #self.depth = depth
        
    #def set_alpha(self, alpha):
        #'''
        #Set the alpha of this node.
        #'''
        #assert(type(alpha) is int or type(alpha) is float)
        #self.alpha = alpha
        
    #def set_beta(self, beta):
        #'''
        #Set the beta of this node.
        #'''
        #assert(type(beta) is int or type(beta) is float)
        #self.beta = beta
        
    #def set_max_turn(self, max_turn):
        #'''
        #Set "self.max_turn".
        #Argument:
        #-- max_turn: bool
        #'''
        #assert(type(max_turn) is bool)
        #self.max_turn = max_turn
        
    # Helper function.
    def _is_node(self, node):
        '''
        Return True if 'node' is a valid representation of a connect four 
        board that can be used in Minimax algorithm.  In other words, 'node'
        needs to satisfy the description in the class documentation.
        Argument:
        -- node: A list of lists of integers.
        Return:
        -- bool
        '''
        if type(node) is not list:
            return False
        row = len(node)
        if row == 0:
            return False
        
        # row > 0
        if not type(node[0]) is list:
            return False  
        col = len(node[0])
        if col == 0:
            return False
        # col > 0
        for i in range(row):
            if type(node[i]) is not list:
                return False
            if not len(node[i]) == col:
                return False
            for j in range(col):
                if not node[i][j] in [1, -1, 0]:
                    return False
        return True
    
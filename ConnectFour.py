'''
Implement class "ConnectFour"
'''

from ConnectFourBoard import *

class ConnectFourError(Exception):
    pass

class ConnectFour:
    '''
    This class implements the functionality to play a Connect Four game.
    
    Reasoning:
    This class will focus the functionality of playing a game.  It uses an
    instance of "ConnectFourBoard", and focus on game-play itself.  It will not
    implement anything related to user interface.
    '''
    def __init__(self, row = 6, col = 7):
        '''
        Set up game play.
        '''
        self.board = ConnectFourBoard(row, col)
        # All the moves made by two players (a list of tuples that give the
        # coordinates of the discs each player have placed.)
        # They are used if a user wants to undo a move.
        self.player1_moves = []
        self.player2_moves = []
        # The state value from 'ConnectFourBoard' class.
        # Dictionary values are chosen based on how 'ConnectFourBoard' 
        # represents board state, described in class docstring.
        self.__in_board_state = {'player1' : 'o', 'player2' : 'x', \
                                 'empty' : '|'}      

    # Accessors:
    def get_col(self):
        '''
        Return the number of columns in the board.
        '''
        return self.board.get_col()
    
    def get_row(self):
        '''
        Return the number of rows in the board.
        '''
        return self.board.get_row()
    
    def get_current_player(self):
        '''
        Return 'player1' if it's player1's turn; and 'player2' if not.
        '''
        return self.board.get_current_player()
    
    def get_available_columns(self):
        '''
        Get non-full columns on the current board.
        Argument: None
        Return:
        -- columns: a list of integer
        '''       
        return self.board.get_available_columns()
    
    def convert_board(self, player1_symbol, player2_symbol, empty_symbol):
        '''
        Convert current board configuration in 'self.board' into another 
        representation.  Replace the symbol for player1 in 'self.board' with
        'player1_symbol'.  Do similar things for player2 and empty locations.
        Argument:
        -- player1_symbol: anything (a character or an integer mostly.)
        -- player2_symbol: anything
        -- empty_symbol: anything
        Return:
        -- new_board: A list of lists with the following meanings.
           new_board[i] represents ith row from the bottom (bottow row is 0th)
           new_board[i][j] represents the current status of the location at
           ith row and jth column from left (very left one is 0th column)
           if new_board[i][j] is player1_symbol, then that location is occupied
           by player1.  Similarly for player2's, and empty locations.
        '''
        row = self.board.get_row()
        col = self.board.get_col()
        new_board = []
        for i in range(row):
            new_board.append([0] * col)
        
        for i in range(row):
            for j in range(col):
                if self.board.get_board_state(i, j) == \
                   self.__in_board_state["player1"]: 
                    new_board[i][j] = player1_symbol
                elif self.board.get_board_state(i, j) == \
                     self.__in_board_state["player2"]: 
                    new_board[i][j] = player2_symbol
                else:
                    new_board[i][j] = empty_symbol    
        return new_board

    # Utility:   
    def make_move(self, disc_col):
        '''
        Update the local instance based on the move 'disc_col'.
        '''
        # Check if 'disc_col' is a valid move.
        assert(disc_col in self.board.get_available_columns())
        disc_row = self.board.get_top_location(disc_col)        
        if self.board.get_current_player() is "player1":
            self.player1_moves.append((disc_row, disc_col))
        else:
            self.player2_moves.append((disc_row, disc_col))            
        self.board.add_to_column(disc_col)
    
    def undo(self):
        '''
        Update the local instance if last move is cancelled.
        '''
        if self.board.get_current_player() is "player1":
            # Then last move was made by "player2"
            if self.player2_moves == []:
                raise ConnectFourError("No moves have been made.")
            else:
                # Remove the last move
                self.player2_moves.pop()
        if self.board.get_current_player() is "player2":
            if self.player1_moves == []:
                raise ConnectFourError("No moves have been made.")
            else:
                self.player1_moves.pop()                
        self.board.remove_last_disc()
    
    def someone_wins(self):
        '''
        Return True if current player wins with current 'moves'.
        'moves' is a list of tuples (row, col)
        '''
        # Only need to check if the newest added disc results in four discs in
        # a row, column, or diagonal.
        
        # Find out who made the last move.
        if self.board.get_current_player() is "player1":
            # player2 made the last move
            moves = self.player2_moves
            disc_type = self.__in_board_state["player2"]
        else:
            # player1 made the last move
            moves = self.player1_moves
            disc_type = self.__in_board_state["player1"]          
        
        if len(moves) == 0:
            # Newly loaded game is not a finished game.  So, an empty moves in
            # a loaded game doesn't violate this decision that no one wins.
            return False
        new_disc = moves[-1]
        # Four directions at 'new_disc'.
        # (0, 1) gives the horizontal direction, because neighboring locations
        # are generated by adding to or subtracting from 'new_disc' by (0, 1).
        # Similarly, (1, 0) is for vertical direction; (1, 1) is for 45-degree
        # diagonal; (-1, 1) is for 135-degree diagonal.
        direction = [(0, 1), (1, 0), (1, 1), (-1, 1)]
        for direc in direction:
            if self._four_discs_connected(new_disc, direc, disc_type):
                return True
        return False
   
    # Helper functions (private methods)
    def _four_discs_connected(self, starting_loc, direction, disc_type):
        '''
        Return True if there are four discs of the same type including the disc 
        on 'starting_loc' in the given direction.
        Arguments:
        -- 'starting_loc' is a tuple, representing a location on the board.
        -- 'direction' is a tuple.  Below is how to interpret it.
        (0, 1) gives the horizontal direction, because neighboring locations
        are generated by adding to or subtracting from 'starting_loc' by (0, 1).
        Similarly, (1, 0) is for vertical direction; (1, 1) is for 45-degree
        diagonal; (-1, 1) is for 135-degree diagonal.
        -- 'disc_type' is a character.  It's the value in dictionary
           'self.__in_board_state'
        '''
        assert(disc_type in self.__in_board_state.values())
       # print self.board.get_board_state(starting_loc[0], starting_loc[1])
       # print disc_type
        assert(self.board.get_board_state(starting_loc[0], starting_loc[1]) == \
               disc_type)
        row = self.board.get_row()
        col = self.board.get_col()
        connected_discs = [starting_loc] # A list of tuples.        
        # The two locations at both ends of the current connected discs.
        one_end = (starting_loc[0] + direction[0], \
                   starting_loc[1] + direction[1])
        other_end = (starting_loc[0] - direction[0], \
                     starting_loc[1] - direction[1])
        while True:
            if 0 <= one_end[0] < row and \
               0 <= one_end[1] < col and \
               self.board.get_board_state(one_end[0], one_end[1]) == disc_type: 
                # One end is occupied by a disc.
                connected_discs.append(one_end)
                if len(connected_discs) == 4:
                    return True
                one_end = (one_end[0] + direction[0], \
                           one_end[1] + direction[1])
                continue
            elif 0 <= other_end[0] < row and \
               0 <= other_end[1] < col and \
               self.board.get_board_state(other_end[0], other_end[1]) == \
               disc_type: 
                # The other end is occupied by a disc.
                connected_discs.append(other_end)
                if len(connected_discs) == 4:
                    return True
                other_end = (other_end[0] - direction[0], \
                             other_end[1] - direction[1])
                continue
            else:
                return False
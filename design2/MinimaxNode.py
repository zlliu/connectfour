'''
Implement class 'MinimaxNode'.
'''
import copy

class MinimaxNodeError(Exception):
    pass

class MinimaxNode:
    '''
    This class contains a representation of a Connect Four board called 'node', 
    and a value associated with it.
    node is a list of lists.  node[i][j] gives state at row 'i' and column 'j'. 
    Row index starts from bottom to top, and column index starts from left to 
    right.
    'node[i][j]':
    Use '0' to represent empty locations.  
    Use '1' for maximizing player ('max')
    Use '-1' for minimizing player ('mini')
    
    Reasoning for this class:
    The main benefit is that it hides all the details about how a node in the
    game tree generated by Minimax algorithm is implemented.
    Minimax algorithm doesn't need to know this implementation detail.
    Another benefit: No need to check if the 'node' in the Minimax algorithm.
    As long as the Minimax algorithm uses an instance of this class, it 
    guarentee that 'node' is valid, because it checks every time an instance is
    created.  It's like an automatic warranty.
    One more benefit: Can store and compute other information related to a
    'MinimaxNode', like 'available_moves' and 'children'.
    Why do I put 'available_moves' in the constructor?
    Since 'available_moves' corresponds to a 'node', we can
    calculate 'available_moves' once in the constructor and whenever other 
    programs need this, they simply call 'get_available_moves()'.
    So, 'available_moves' can be used many times, but it only needs to be 
    calculated once.
    '''
    # TODO: need a better name to differentiate self.node and MinimaxNode, I 
    # mean, we can't name the board 'self.node' and an instance 'node'.
    # node.get_node() is awkward.
    def __init__(self, node):
        '''
        Construct a new Minimax node.
        '''
        # Question: should we use assertion or exception?  Assertion allows us
        # to improve the performance by disable assertion.
        # exception gives more flexibility.
        if not self._is_node(node):
            raise MinimaxNodeError("Given node is not valid.")
        self.node = node
        self.row = len(node)
        self.col = len(node[0])
        self.available_moves = self._find_available_moves()
        # Default number of 'self.value' is 0.
        # It's meaningless if 'self.evaluation_flag' is False
        self.value = 0
        self.evaluation_flag = False  # Question: A good name?
        
    def is_evaluated(self):
        '''
        Return True if 'self.node' is evaluated.
        '''
        return self.evaluation_flag
    
    def get_node(self):
        '''
        Return the node.
        '''
        return copy.deepcopy(self.node)
    
    def get_value(self):
        '''
        Return the value of this node.
        '''
        if self.evaluation_flag:
            return self.value
        raise MinimaxNodeError("Error: the node hasn't been evaluated.")
    
    def set_value(self, node_value):
        '''
        Set the value of this node.
        '''
        # Question: should we allow 'value' is evaluated more than once?
        self.value = node_value
        self.evaluation_flag = True
        
    def get_available_moves(self):
        '''
        Get available moves on the current board.
        '''
        return copy.deepcopy(self.available_moves)
    
    def find_children(self, max_turn):
        '''
        Find children nodes of the current node 'self.node'.  More specifically,
        generate all the possible boards after one ply from the current board, 
        based on whose turn it is.
        Argument:
        -- max_turn: bool, True if it's max's turn, and False if not.
        Return:
        -- children: a dictionary, whose keys are available moves, and whose 
                     values are instances of 'MinimaxNode', representing the 
                     child node after making a move (its key).
        '''
        # Contract. (Test arguments)
        assert(type(max_turn) is bool)
        children = {}
        for move in self.available_moves: # 'move' is a tuple.
            temp = copy.deepcopy(self.node)
            if max_turn:
                temp[move[0]][move[1]] = 1
            else:
                temp[move[0]][move[1]] = -1                    
            children[move] = MinimaxNode(temp)
        return children
        
    # Helper function.
    def _is_node(self, node):
        '''
        Return True if 'node' is a valid representation of a connect four 
        board that can be used in Minimax algorithm.  In other words, 'node'
        needs to satisfy the description in the class documentation.
        Argument:
        -- node: A list of lists of integers.
        Return:
        -- bool
        '''
        if type(node) is not list:
            return False
        row = len(node)
        if row == 0:
            return False
        
        # row > 0
        if not type(node[0]) is list:
            return False  
        col = len(node[0])
        if col == 0:
            return False
        # col > 0
        for i in range(row):
            if type(node[i]) is not list:
                return False
            if not len(node[i]) == col:
                return False
            for j in range(col):
                if not node[i][j] in [1, -1, 0]:
                    return False
        return True
    
    def _find_available_moves(self):
        '''
        Find available moves on the current board.
        Argument: None.
        Return:
        available_moves-- A list of tuples, representing a location (row, col).
        '''
        # Find out the available next moves.  For each column, find out the top
        # position, which is the lowest row index with value of 0.
        available_moves = []
        # Indices of the columns that the program hasn't find the top positions.
        left_cols = range(self.col)
        for i in range(self.row):
            for j in range(self.col):
                if (j in left_cols) and (self.node[i][j] == 0):
                    available_moves.append((i, j))
                    left_cols.remove(j)
        return available_moves
    
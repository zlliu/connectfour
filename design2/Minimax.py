'''
This module implements 'Minimax' class.
'''
import copy
from ConnectFourBoardEvaluator import *
# from MinimaxNode import *

class Minimax:
    '''
    Implements Minimax algorithm for Connect Four game.
    '''
    
    # TODO: bug: print twice
    def __init__(self):
        '''
        Constructor.
        '''
        self.evaluator = ConnectFourBoardEvaluator()
    
    def next_move(self, current_node, depth = 1):
        '''
        Gives the next move based on the current board ('current_node').
        Argument:
        -- node: an instance of 'MinimaxNode'.
        -- depth: integer, representing how many plies to go ahead.
        Return: 
        -- selected_col: An integer representing the column index.
        '''
        assert(type(depth) is int)
        assert(depth >= 0)
        
        # The function assumes the current player is maximizing player ('max').
        max_turn = True
        maximum = -self.evaluator.get_inf()
        selected_col = -1
        children = current_node.find_children(max_turn)
        for move in children:
            child = children[move]
            # 'child' is the result of making 'move'.
            temp = self.minimax(child, depth - 1, not(max_turn))
            if maximum <= temp:
                maximum = temp
                selected_col = move[1] # only need the column.
        return selected_col

    def minimax(self, node, depth, max_turn):
        '''
        Implement a position evaluation function based on minimax algorithm.
        Argument:
        -- node: an instance of class 'MinimaxNode'.
        -- depth: integer, representing how many plies to go ahead.
        -- max_turn: bool: True if it's max's turn.
        Return:
        -- value: integer, representing the value of 'node'.
        ----- The bigger the value, the better chance for max to win.
        ----- If max wins, the value is +inf; if mini wins, the value is -inf.
        '''
        # Assert on arguments.
        assert(depth >= 0)
        assert(type(depth) is int)
        assert(type(max_turn) is bool)
        if (depth <= 0) or (self.is_terminal(node)):
            return self.evaluator.evaluate(node)
        # TODO: following codes can by made 'DRYER'.
        children = node.find_children(max_turn)
        if max_turn:
            # max chooses the move with maximum value.
            value = -self.evaluator.get_inf()
            for move in children:
                child = children[move]
                value = max(value, self.minimax(child, depth - 1, not(max_turn)))
            return value
        else:
            # mini chooses the move with the minimum value.
            value = self.evaluator.get_inf()
            for move in children:
                child = children[move]
                value = min(value, self.minimax(child, depth - 1, not(max_turn)))
            return value

    def is_terminal(self, node):
        '''
        If the current node is the terminal node, return True.
        '''
        # If the board is full, then it's a terminal node.
        if 0 not in node.get_node()[-1]:  # node[-1] is the very top row.
            return True
        return False

'''
Implement the class 'ConnectFourBoard'.
'''
import copy
import sys

class ConnectFourBoardError(Exception):
    pass

class ConnectFourFileError(ConnectFourBoardError):
    pass

class ConnectFourBoard:
    '''
    This class represents Connect Four board, and handles basic operations.    
    '''
    def __init__(self, row = 6, col = 7):
        '''
        Constructor: initialize internal state variables.
        '''        
        self.row = row
        self.col = col
        # A list of 'row' lists with length 'col'.  So, self.game[i][j] gives
        # state at row 'i' and column 'j'.  Row index starts from bottom to top.
        # Column index starts from left to right.
        # Use '0' to represent empty locations.  Use '1' for player1 and '2' for
        # player2.
        self.game = []
        for i in range(self.row):
            self.game.append([0] * self.col)
        # 'available_moves[i]' gives next available location(index) at column i.
        self.available_moves = [0] * self.col
        self.player1_turn = True  # Player 1 always goes first.
        # A dictionary about which peg does each player use. (constant)
        # Question: How to make a variable constant
        self.player_peg = {'player1' : 'o', 'player2' : 'x'} # 'o' goes first.
        # By default, player 1 is human, and player 2 is AI.
        self.player_type = {'player1_is_human' : True, 
                            'player2_is_human' : False}
        # All the moves made by two players (a list of tuples that give the
        # coordinates of the pegs each player have placed.)
        self.player1_moves = []
        self.player2_moves = []
        
    def get_col(self):
        '''
        Return the number of columns in the board.
        '''
        return self.col
    
    def get_row(self):
        '''
        Return the number of rows in the board.
        '''
        return self.row
    
    def get_board(self):
        '''
        Return current board as a list of lists.
        '''
        return copy.deepcopy(self.game)  # Avoid aliasing.
    
    def get_available_moves(self):
        '''
        Return the available moves: a list of integers (row indices).
        '''
        return copy.copy(self.available_moves)  # Avoid aliasing.
    
    def get_current_player(self):
        '''
        Return 'player1' if self.player1_turn is True; and 'player2' if False.
        '''
        if self.player1_turn:
            return 'player1'
        return 'player2'
    
    def get_player1_type(self):
        '''
        Return the player1's type.
        Return:
        -- A string either 'human' or 'computer'.
        '''
        if self.player_type['player1_is_human']:
            return 'human'
        return 'computer'
            
    def get_player2_type(self):
        '''
        Return the player2's type.
        Return:
        -- A string either 'human' or 'computer'.
        '''
        if self.player_type['player2_is_human']:
            return 'human'
        return 'computer'    
    
    # Question: How to forbid other program from setting the following data
    # incorrectly?
    def set_player1_type(self, type_string):
        '''
        Set the values in dictionary 'self.player_type'.
        Argument:
        -- type_string: A string be either 'human' or 'computer'.
        '''
        if type_string is 'human':
            self.player_type['player1_is_human'] = True
        elif type_string is 'computer':
            self.player_type['player1_is_human'] = False
        else:
            raise ConnectFourBoardError \
                  ("Player type string must be either 'human' or 'computer'.")

    def set_player2_type(self, type_string):
        '''
        Set the values in dictionary 'self.player_type'.
        Argument:
        -- type_string: A string be either 'human' or 'computer'.
        '''
        if type_string is 'human':
            self.player_type['player2_is_human'] = True
        elif type_string is 'computer':
            self.player_type['player2_is_human'] = False
        else:
            raise ConnectFourBoardError \
                  ("Player type string must be either 'human' or 'computer'.")
    
    def set_who_goes_first(self, player_string):
        '''
        Set the value in 'self.player1_turn' to True if 'player_string' is 
        'player1' or False if 'player2'.
        '''
        if player_string is 'player1':
            self.player1_turn = True
        elif player_string is 'player2':
            self.player1_turn = False
        else:
            raise ConnectFourBoardError \
                  ("Player string must be either 'player1' or 'player2'.")

    def valid_move(self, peg_col):
        '''
        Return True if 'peg_col' is a valid move; False and printing messages
        if 'peg_col' is not a valid move.
        '''
        if peg_col not in range(self.col):
            sys.stderr.write('Error: Input is not in range.\n')
            return False
        if self.available_moves[peg_col] >= self.row:
            sys.stderr.write('Error: No pegs can be put on selected column!\n')
            return False
        return True

    def make_move(self, peg_col):
        '''
        Update the local instance based on the move 'peg_col'.
        '''
        # Check if 'peg_col' is a valid move.
        assert(self.valid_move(peg_col))
        
        peg_row = self.available_moves[peg_col]
        # Find out whose turn it is.
        if self.player1_turn:
            self.game[peg_row][peg_col] = 1
            self.player1_moves.append((peg_row, peg_col))
            # Switch to player2.
            self.player1_turn = False
        else:
            self.game[peg_row][peg_col] = 2
            self.player2_moves.append((peg_row, peg_col))            
            # Switch to player1.
            self.player1_turn = True
        self.available_moves[peg_col] = peg_row + 1
        
    def undo(self):
        '''
        Update the local instance if last move is cancelled.
        '''
        pass
    
    def someone_wins(self):
        '''
        Return True if current player wins with current 'moves'.
        'moves' is a list of tuples (peg_row, peg_col)
        '''
        # Only need to check if the newest added peg results in four pegs in
        # a row, column, or diagonal.
        
        # Find out who made the last move.
        if self.player1_turn:
            # player2 made the last move
            moves = self.player2_moves
            peg_type = 2
        else:
            # player1 made the last move
            moves = self.player1_moves
            peg_type = 1            
        
        if len(moves) == 0:
            # Newly loaded game is not a finished game.  So, an empty moves in
            # a loaded game doesn't violate this decision that no one wins.
            return False
        new_peg = moves[-1]
        # Four directions at 'new_peg'.
        # (0, 1) gives the horizontal direction, because neighboring locations
        # are generated by adding to or subtracting from 'new_peg' by (0, 1).
        # Similarly, (1, 0) is for vertical direction; (1, 1) is for 45-degree
        # diagonal; (-1, 1) is for 135-degree diagonal.
        direction = [(0, 1), (1, 0), (1, 1), (-1, 1)]
        for direc in direction:
            if self._four_pegs_connected(new_peg, direc, peg_type):
                return True
        return False
    
    def convert_board(self, player1_symbol, player2_symbol, empty_symbol):
        '''
        Convert current board configuration 'self.game' into another 
        representation.  Replace the symbol for player1 in 'self.game' with
        'player1_symbol'.  Do similar things for player2 and empty locations.
        Argument:
        -- player1_symbol: anything (a character or an integer mostly.)
        -- player2_symbol: anything
        -- empty_symbol: anything
        Return:
        -- new_board: A list of lists with the following meanings.
           new_board[i] represents ith row from the bottom (bottow row is 0th)
           new_board[i][j] represents the current status of the location at
           ith row and jth column from left (very left one is 0th column)
           if new_board[i][j] is player1_symbol, then that location is occupied
           by player1.  Similarly for player2's, and empty locations.
        '''
        new_board = copy.deepcopy(self.game)
        for i in range(self.row):
            for j in range(self.col):
                if new_board[i][j] == 1: # 1 means player1 in 'self.game'
                    new_board[i][j] = player1_symbol
                elif new_board[i][j] == 2:
                    new_board[i][j] = player2_symbol
                else:
                    new_board[i][j] = empty_symbol     
        return new_board
    
    # Question: should I put load and save into ConnectFour class or this class?
    def load(self):
        '''
        Load a previously saved game.
        '''
        # Loaded game is not finished because saving doesn't allow that.
        # So, no need to test if someone wins on a newly loaded game.
        print "loading game..."
        file_suffix = '.c4' # Avoid other files being opened.
        while True:
            try:
                filename = raw_input("Enter the file name (.c4): ")                
                myfile = open(filename + file_suffix, 'r')
                # Load everything into temporary variables.
                # Don't change the data members until the end, when no file
                # format errors happen.
                first_line = myfile.readline()
                if first_line == '1\n':
                    player1_turn = True
                elif first_line == '2\n':
                    player1_turn = False
                else:
                    raise ConnectFourFileError("First line is invalid.")
                
                second_line = myfile.readline()
                if len(second_line) != 3: # including "\n"
                    raise ConnectFourFileError("Second line has invalid length.")
                    
                if second_line[0] == 'h':
                    player1_is_human = True
                elif second_line[0] == 'c':
                    player1_is_human = False
                else:
                    raise ConnectFourFileError \
                          ("Second line has invalid first character.")

                if second_line[1] == 'h':
                    player2_is_human = True
                elif second_line[1] == 'c':
                    player2_is_human = False
                else:
                    raise ConnectFourFileError \
                          ("Second line has invalid second character.")
                
                board = []
                while True:
                    line = myfile.readline()
                    if line == '':
                        break;
                    if len(line) != (self.col + 1): # including "\n"
                        raise ConnectFourFileError \
                              ("A line has invalid length.")                              

                    for char in line:
                        if not char in ['0', '1', '2', '\n']:
                            raise ConnectFourFileError \
                                  ("A line has invalid character.")
                                  
                            
                    current_row = []
                    for i in range(self.col):
                        current_row.append(int(line[i]))
                    board.append(current_row)
                    
                
                if len(board) != self.row:
                    raise ConnectFourFileError \
                          ("Wrong number of rows.")
                
                if not self._is_valid_board(board):
                    raise ConnectFourFileError \
                          ("Not a valid game board.")
                    
                myfile.close()
                break
            except IOError:
                print "File " + filename + file_suffix + " doesn't exist."
                print "Please give a valid filename."
            except ConnectFourFileError, e:
                print "Invalid file format."
                print e
                print "Please give another valid file (.c4)."

        # Copy temporary variables to data members.
        # TODO: should make loading be able to load a game with different row
        # and column.
        self.game = copy.deepcopy(board)
        self.player1_turn = player1_turn
        self.player_type["player1_is_human"] = player1_is_human
        self.player_type["player2_is_human"] = player2_is_human
        self.player1_moves = []
        self.player2_moves = []                
        self._find_next_moves()

    def save(self):
        '''
        Save the current game to a file with following format.
        1
        hc
        2211222
        0021221
        0021000
        0002000
        0001000
        0000000
        First line: 1 or 2 (1 if player1's turn; 2 if player2's turn.)
        Second line: hc or hh or ch (gives players types eg. hc means player1
                                     is human, while player2 is computer)
        Following lines: board configuration upside down
                         (0: empty; 1: occupied by player1;
                          2: occupied by player2)
        '''
        # Current program doesn't allow saving a finished game.
        print "Saving game..."
        filename = raw_input("Enter the file name: ")
        file_suffix = '.c4'
        myfile = open(filename + file_suffix, 'w')
        if self.player1_turn:
            myfile.write("1\n")
        else:
            myfile.write("2\n")
        if self.player_type["player1_is_human"]:
            myfile.write('h')
        else:
            myfile.write('c')
        if self.player_type["player2_is_human"]:
            myfile.write("h\n")
        else:
            myfile.write("c\n")
        for i in range(self.row):
            for j in range(self.col):
                myfile.write(str(self.game[i][j]))
            myfile.write("\n")
        myfile.close()
                   
    def show(self):
        '''
        Pretty-print the current game.
        '''
        print
        print "=================="
        print "   Connect Four"
        # Print header.
        sys.stdout.write('   ')
        for i in range(self.col):
            sys.stdout.write('%d ' % i)
        print
        # Print game.
        for i in range(self.row - 1, -1, -1):
            sys.stdout.write(' %d ' % i)
            for j in range(self.col):
                if self.game[i][j] == 0:
                    sys.stdout.write('| ')
                elif self.game[i][j] == 1:
                    sys.stdout.write('%s ' % self.player_peg['player1'])
                elif self.game[i][j] == 2:
                    sys.stdout.write('%s ' % self.player_peg['player2'])
                else:
                    raise ConnectFourError('Incorrect game configuration.')
            print
        if self.player_type['player1_is_human']:
            player1_type_str = 'human'
        else:
            player1_type_str = 'computer'
        if self.player_type['player2_is_human']:
            player2_type_str = 'human'
        else:
            player2_type_str = 'computer'
        print "player1 (%s): %s" % \
              (self.player_peg['player1'], player1_type_str)
        print "player2 (%s): %s" % \
              (self.player_peg['player2'], player2_type_str)
        print "=================="                
        print
        
    # Helper functions (private methods)
    def _is_valid_board(self, board):
        '''
        Check if the given game configuration 'board' is valid.
        '''
        # TODO.
        return True
    
    def _four_pegs_connected(self, starting_loc, direction, peg_type):
        '''
        Return True if there are four pegs of the same type including the peg 
        on 'starting_loc' in the given direction.
        Arguments:
        -- 'starting_loc' is a tuple, representing a location on the board.
        -- 'direction' is a tuple.  Below is how to interpret it.
        (0, 1) gives the horizontal direction, because neighboring locations
        are generated by adding to or subtracting from 'starting_loc' by (0, 1).
        Similarly, (1, 0) is for vertical direction; (1, 1) is for 45-degree
        diagonal; (-1, 1) is for 135-degree diagonal.
        -- 'peg_type' is an integer: 1 represents player1's pegs; 
                                     2 represents player2's pegs.
        '''
        assert(self.game[starting_loc[0]][starting_loc[1]] == peg_type)
        
        connected_pegs = [starting_loc] # A list of tuples.        
        # The two locations at both ends of the current connected pegs.
        one_end = (starting_loc[0] + direction[0], \
                   starting_loc[1] + direction[1])
        other_end = (starting_loc[0] - direction[0], \
                     starting_loc[1] - direction[1])
        while True:
            if 0 <= one_end[0] < self.row and \
               0 <= one_end[1] < self.col and \
               self.game[one_end[0]][one_end[1]] == peg_type: 
                # One end is occupied by a peg.
                connected_pegs.append(one_end)
                if len(connected_pegs) == 4:
                    return True
                one_end = (one_end[0] + direction[0], \
                           one_end[1] + direction[1])
                continue
            elif 0 <= other_end[0] < self.row and \
               0 <= other_end[1] < self.col and \
               self.game[other_end[0]][other_end[1]] == peg_type: 
                # The other end is occupied by a peg.
                connected_pegs.append(other_end)
                if len(connected_pegs) == 4:
                    return True
                other_end = (other_end[0] - direction[0], \
                             other_end[1] - direction[1])
                continue
            else:
                return False
            
    def _find_next_moves(self):
        '''
        Find next moves on the current board.
        '''
        # Find out the available next moves.  For each column, find out the top
        # position, which is the lowest row index with value of 0.
        # Available next moves: a list of integers.  next_moves[i] is the next 
        # row index at column 'i'.
        next_moves = [0] * self.col
        # Indices of the columns that the program hasn't find the top positions.
        left_cols = range(self.col)
        for i in range(self.row):
            for j in range(self.col):
                if (j in left_cols) and (self.game[i][j] == 0):
                    next_moves[j] = i
                    left_cols.remove(j)
        # TODO: Check if available_moves is correct.
        self.available_moves = next_moves
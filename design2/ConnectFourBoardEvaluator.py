'''
This module implements class 'ConnectFourBoardEvaluator'.
'''

class ConnectFourBoardEvaluator:
    '''
    This class performs static Connect Four board evaluation in the Minimax
    algorithm.
    
    Reasoning for this class: 
    In Minimax algorithm, we only need to construct one single evaluator, 
    and then simply use this evaluator to evaluate all the Minimax nodes.
    Therefore, we only need to generate 'four_connected_holes' once in the 
    constructor.  Thus avoid having to make it a global variable in the old 
    Minimax module.
    '''
    def __init__(self, row = 6, col = 7):
        '''
        Construct an evaluator.
        '''
        # Pre-compute 'self.four_connected_holes'.
        # 'self.four_connected_holes' is a frozenset of frozensets of 4 tuples.  
        # Each tuple represents a location (row, col) on the board.
        # Each frozenset of 4 tuples represents four connected locations.       
        self._generate_four_connected_holes(row, col)
        # Use a very large integer to represents infinity in Minimax algorithm.
        self.inf = 10000000
    
    def get_inf(self):
        '''
        Get the number representing infinity.
        '''
        return self.inf
    
    def evaluate(self, node):
        '''
        Evaluate a Connect Four board, represented as in 'MinimaxNode'.
        Argument:
        node-- An instance of 'MinimaxNode'.
        Return:
        value-- An integer.
        '''
        value = 0
        for ele in self.four_connected_holes:
            # 'ele' is a frozenset of 4 tuples.  
            # Each tuple gives the position (row, col)
            pegs_position = []
            for position in ele:
                pegs_position.append(node.get_node()[position[0]][position[1]])
            value += self._pegs_value(pegs_position)
        # Make sure 'value' is within (-inf, inf).
        if value < -self.inf:
            value = -self.inf
        elif value > self.inf:
            value = self.inf
        return value
    
    # Helper functions.
    def _pegs_value(self, pegs):
        '''
        Return the heuristic value of the pegs in any 4 connected holes.
        '''
        assert(type(pegs) is list)
        assert(len(pegs) == 4)
        # If four pieces are connected, return +inf for max, -inf for mini.
        four = self.inf
        # If three pieces are connected, 1000 for max, -1000 for mini.
        three = 1000
        # If two pieces are connected, 10 for max, -10 for mini.
        two = 10
        # If one piece.
        one = 1
        # Otherwise
        zero = 0
        
        num_pos = 0  # Number of +1s
        num_neg = 0  # Number of -1s
        for i in range(4):
            if pegs[i] == 1:
                num_pos += 1
            elif pegs[i] == -1:
                num_neg += 1
        if (num_pos == 4):
            return four
        if (num_pos == 3) and (num_neg == 0):
            return three
        if (num_pos == 2) and (num_neg == 0):
            return two
        if (num_pos == 1) and (num_neg == 0):
            return one
        if (num_neg == 4):
            return -four
        if (num_neg == 3) and (num_pos == 0):
            return -three
        if (num_neg == 2) and (num_pos == 0):
            return -two
        if (num_neg == 1) and (num_pos == 0):
            return -one
        return zero
    
    def _generate_four_connected_holes(self, row, col):
        '''
        Generate all the four connected holes on a 'row' by 'col' Connect Four 
        board.
        The function modifies 'self.four_connected_holes' in place.
        Arguments:
        -- row: integer
        -- col: integer
        Return: None.
        '''
        # Initially a list, but will be converted to frozenset.
        four_connected_holes = []
        
        for i in range(row):
            for j in range(col - 4 + 1):
                four_connected_holes.append(frozenset([(i, j), (i, j + 1), \
                                                       (i, j + 2), (i, j + 3)]))
        # four in a column.
        for i in range(col):
            for j in range(row - 4 + 1):
                four_connected_holes.append(frozenset([(j, i), (j + 1, i), \
                                                       (j + 2, i), (j + 3, i)]))
        
        # four in a diagonal: NE - SW
        for i in range(row - 4 + 1):
            for j in range(col - 4 + 1):
                four_connected_holes.append(frozenset([(i, j), \
                                                       (i + 1, j + 1), \
                                                       (i + 2, j + 2), \
                                                       (i + 3, j + 3)]))
        # four in a diagonal: NW - SE
        for i in range(row - 4 + 1, row):
            for j in range(col - 4 + 1):
                four_connected_holes.append(frozenset([(i, j), \
                                                       (i - 1, j + 1), \
                                                       (i - 2, j + 2), \
                                                       (i - 3, j + 3)]))
        self.four_connected_holes = frozenset(four_connected_holes)    
        # Reason for choosing frozensets.
        # Use frozenset to store 'four_connected_holes'.
        # 1st, Use list.  But when creating the list, make the element a frozenset.
        # After creating the list, convert it to frozenset.
        # The reason is you don't want to change them later on.  They are pre-computed.
        # Tuples are immutable, so can be used as elements in a set.
        # element of set has to be hashable, so lists and sets can't be the elements.
        # But frozensets can be the elements.
        # So, a set of frozensets
        # If we have two sets of sets, then it's easy to compare.
        # simply run A == B, then if A and B contain the same subsets, it returns True.
        # Because in the set, the elements are not ordered, and are unique.
        # So, no need to worry about the different order of when you write the set.
        # four in a row.    

    
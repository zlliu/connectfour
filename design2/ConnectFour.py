'''
This program allows the user to interactively play the game of Connect Four.
'''

import sys
import random
import copy
from ConnectFourBoard import *
from MinimaxNode import *
from Minimax import *

class ConnectFourError(Exception):
    pass

class ConnectFourMoveError(ConnectFourError):
    pass

class ConnectFourCommandError(ConnectFourError):
    pass

class ConnectFour:
    '''
    Interactively play the game of ConnectFour.
    
    Reasoning about this class:
    This class needs to know who is playing against who (player types)
    and who is going first in order to kick off the game.
    It doesn't need to know whose turn it is now, and any details about
    the board.
    This class needs to handle interactions with human, and focus
    on the game interface.
    It also handles what algorithm to use to make a move for the computer.
    '''

    def __init__(self, row = 6, col = 7):
        '''
        Constructor: initialize internal state variables.
        '''
        # An instance of ConnectFourBoard.
        self.board = ConnectFourBoard()
        # Game mode:
        self.game_mode = 1  # By default: human vs. computer
        self.minimax = Minimax()

    def user_input(self):
        '''
        Ask user for proper input.
        Return:
        -- A string, representing either a command or a column number.
        '''
        while True:
            print 
            print 'Enter the column number: 0 - ' + \
                  str(self.board.get_col() - 1)
            print 'Or use following commands:'
            print 's: Save current game.'
            print 'l: Load previous saved game.'
            print 'h: give a Hint.'
            print 'e: Exit game.'
            try:
                user_cmd = raw_input('Please make your move: ')
                if user_cmd in ['s', 'l', 'h', 'e']:
                    return user_cmd
                else:
                    selected_col = int(user_cmd)
                    if not self.board.valid_move(selected_col):
                        raise ConnectFourMoveError('Error: Invalid column!')
                    return str(selected_col)
            except ValueError:
                print 'Error: Invalid input!'
            except ConnectFourMoveError, e:
                print e
            
            self.board.show()

    def play_one_game(self):
        '''
        Play the game for one time.
        '''
        while True:
            self.board.show()
            # Ask for inputs.
            if self.board.get_current_player() is 'player1':
                if self.board.get_player1_type() is 'human':
                    print "Player1's turn:"
                    current_player_is_human = True
                else:
                    current_player_is_human = False                    
            else:
                if self.board.get_player2_type() is 'human':
                    print "Player2's turn:"
                    current_player_is_human = True
                else:
                    current_player_is_human = False                    

            if current_player_is_human:
                user_cmd = self.user_input()
                # Question: should my program trust the output of 'user_input()'
                # or should I check if output is valid here?
                # Handle user command
                if user_cmd == 's':
                    self.board.save()
                    continue
                elif user_cmd == 'l':
                    self.board.load()
                    continue
                elif user_cmd == 'h':
                    peg_col = self.hint()
                elif user_cmd == 'e':
                    break
                elif int(user_cmd) in range(self.board.get_col()):
                    peg_col = int(user_cmd)
                else:
                    sys.stderr.write("Don't know how to handle user command: " \
                                     + user_cmd)
                    continue
            else:
                peg_col = self.select_col(2)

            # Make move:
            self.board.make_move(peg_col)                
            # Display:
            self.board.show()
            # Check if player2 wins.
            if self.board.someone_wins():
                if self.board.get_current_player() is 'player1':
                    # Last player is player2.
                    print "Congratulations, Player2!  You win!"
                else:
                    print "Congratulations, Player1!  You win!"
                break


    def play_game(self):
        '''
        Play game continuously.
        '''
        # Play game.
        play_game = True
        while play_game:
            self.play_one_game()
            while True:
                user_input = raw_input \
                    ("Do you want to play one more game? (y/n): ")
                if user_input == 'y':
                    play_game = True
                    last_player1_type = self.board.get_player1_type()
                    last_player2_type = self.board.get_player2_type()
                    # Initialize a new game.
                    self.board = ConnectFourBoard()
                    if self.game_mode == '1':
                        # Switch the order of first moves if playing against AI.
                        self.board.set_player1_type(last_player2_type)
                        self.board.set_player2_type(last_player1_type)
                    else:
                        self.board.set_player1_type(last_player1_type)
                        self.board.set_player2_type(last_player2_type)
                    break
                if user_input == 'n':
                    play_game = False
                    break
                print "Invalid input!"
                
    def run(self):
        '''
        Play Connect Four games.
        '''
        # Welcoming message.
        print 'Play Connect 4!'
        
        # Select game mode.
        while True:
            print 'Game mode:'
            print '1. Against a computer'
            print '2. Between two human'            
            self.game_mode = raw_input("Select game mode (1/2): ")
            if self.game_mode == '1': # default choice
                self.board.set_player1_type('human')
                self.board.set_player2_type('computer')
                self.board.set_who_goes_first('player1')
                break
            if self.game_mode == '2':
                self.board.set_player1_type('human')
                self.board.set_player2_type('human')
                self.board.set_who_goes_first('player1')
                break
            print 'Invalid input!'
        self.play_game()
           
        # Advanced features:
        # 1.Ask user what kind of game to play. (7x6 or 8x7 etc..)
        # 2.Ask user to select difficulty levels.
        # 3.Add command: give up.
        # 4.Add info about history records.
        # 5.Make something easier for human than level 2
        
    # TDODO: Better name for 'select_col'.
    def select_col(self, level = 0):
        '''
        Selects a column number based on minimax algorithm.
        '''
        if level == 0:
            # Implement stupid computer by randomly selecting columns.
            random.seed()
            while True:
                selected_col = random.randint(0, self.board.get_col() - 1)
                if self.board.get_available_moves()[selected_col] < \
                   self.board.get_row():
                    return selected_col
        else:
            if self.board.get_current_player() is 'player1':
                # Player1 is maximizing player.
                node = self.board.convert_board(1, -1, 0)
            else:
                node = self.board.convert_board(-1, 1, 0)             
            return self.minimax.next_move(MinimaxNode(node), level)
           
    def undo(self):
        '''
        Undo the current move.
        '''
        # TODO
        pass
    
    def hint(self):
        '''
        Give a hint for next move.
        Return:
        -- selected_col: A integer
        '''
        # TODO
        pass    
    
if __name__ == '__main__':
    s = ConnectFour()
    s.run()


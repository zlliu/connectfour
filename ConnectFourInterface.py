'''
This program allows the user to interactively play the game of Connect Four.
'''

import sys
import random
import copy
from ConnectFour import *
from ConnectFourFile import *
from Minimax import *

class ConnectFourInterfaceError(Exception):
    pass

class ConnectFourMoveError(ConnectFourInterfaceError):
    pass

class ConnectFourCommandError(ConnectFourInterfaceError):
    pass

class ConnectFourInterface:
    '''
    Interactively play the game of ConnectFour.
    Depends on 'ConnectFour' and 'ConnectFourFile' classes.
    
    Reasoning about this class:
    This class needs to know who is playing against who (player types)
    and who is going first in order to kick off the game.
    It doesn't need to know whose turn it is now, and any details about
    the board/game.
    This class needs to handle interactions with human, and focus
    on the game interface.
    It also handles what algorithm to use to make a move for the computer.
    '''
    # TODO: go though this class: make sure everything is right and clean up
    # the codes if necessary.  It seems the abstraction is not very clear 
    # right now.  It's not clear if I can totally ignore the details of other 
    # class.  Need to figure this out.
    # TODO: Keep a record of game history.  And print the record when people 
    # exit the game.
    # TODO: right now, whenever we add a new command to the interface, we need
    # to update two places in "user_input" and one place in "play_one_game".
    # TODO: need to have test file that simulate the interactions people may
    # have with the game, basically test out all commands, options and everything
    # So that if in the future, I change any thing, I can run the test file 
    # istead of testing all user interactions myself.
    # TODO: Need to pass the entire class to save or load, not just the game.
    # need to rewrite the save, load part.
    def __init__(self, row = 6, col = 7):
        '''
        Constructor: initialize internal state variables.
        '''
        # An instance of ConnectFour.
        self.game = ConnectFour(row, col)
        self.game_file = ConnectFourFile()
        self.minimax = Minimax(row, col) 
        self.row = row
        self.col = col
        # Game mode:
        self.game_mode = 1  # By default: human vs. computer
        # So, as for class "ConnectFour" or more specifically "ConnectFourBoard"
        # it doesn't need to know players choices of types, discs, or names.
        # It simply players a two players game, and always let player1 go first,      
        # and internally keep track of whose turn it is.
        # What this class does is to keep track of playes' types, discs and
        # names for one series of game.  It changes who goes first by switchving
        # players' types.  Player s' discs and names are changed accordingly so
        # make sure one player always show the same disc and name.
        # So, as for class "ConnectFour" or "ConnectFourBoard", every game is
        # the same.
        self.player_type = {'player1' : "human", 'player2' : "computer"}
        self.player_disc = {'player1' : 'o', 'player2' : 'x'}        
        self.player_name = {"player1" : "player1", "player2" : "player2"}

    def show(self):
        '''
        Pretty-print the current game.
        '''
        # Convert board into a list of lists of integers.
        # 1 means player1's discs; 2 means player2's; 0 means empty.
        board = self.game.convert_board(1, 2, 0)
        col = self.game.get_col()
        row = self.game.get_row()
        print
        print "=================="
        print "   Connect Four"
        # Print header.
        sys.stdout.write('   ')
        for i in range(col):
            sys.stdout.write('%d ' % i)
        print
        # Print game.
        for i in range(row - 1, -1, -1):
            sys.stdout.write(' %d ' % i)
            for j in range(col):
                if board[i][j] == 0:
                    sys.stdout.write('| ')
                elif board[i][j] == 1:
                    sys.stdout.write('%s ' % self.player_disc['player1'])
                elif board[i][j] == 2:
                    sys.stdout.write('%s ' % self.player_disc['player2'])
                else:
                    raise ConnectFourInterfaceError \
                          ('Incorrect game configuration.')
            print
  
        player1_name = self.player_name["player1"]
        player2_name = self.player_name["player2"]
        print "%s: %s" % (player1_name, self.player_disc['player1'])
        print "%s: %s" % (player2_name, self.player_disc['player2'])
        print "=================="                
        print
        
    def user_input(self):
        '''
        Ask user for proper input.
        Return:
        -- A string, representing either a command or a column number.
        '''
        while True:
            print 
            print 'Enter the column number: 0 - ' + \
                  str(self.game.get_col() - 1)
            print 'Or use following commands:'
            print 'u: Undo last move'
            print 'h: give a Hint.'            
            print 's: Save current game.'
            print 'l: Load previous saved game.'
            print 'e: Exit game.'
            try:
                user_cmd = raw_input('Please make your move: ')
                if user_cmd in ['u', 'h', 's', 'l', 'e']:
                    return user_cmd
                else:
                    selected_col = int(user_cmd)
                    if selected_col not in self.game.get_available_columns():
                        raise ConnectFourMoveError('Error: Invalid column!')
                    return str(selected_col)
            except ValueError:
                print 'Error: Invalid input!'
            except ConnectFourMoveError, e:
                print e
            
            self.show()

    def play_one_game(self):
        '''
        Play the game for one time.
        '''
        while True:
            self.show()
            if self.__current_player_type() is "human":
                user_cmd = self.user_input()
                # Handle user command
                if user_cmd == 'u':
                    self.__undo()
                    continue
                elif user_cmd == 'h':
                    disc_col = self.select_col(4)
                elif user_cmd == 's':
                    self.game_file.save(self.game)
                    continue
                elif user_cmd == 'l':
                    self.__load()
                    continue                
                elif user_cmd == 'e':
                    if self.__exit_game():
                        break
                    else:
                        continue
                elif int(user_cmd) in range(self.game.get_col()):
                    disc_col = int(user_cmd)
                else:
                    sys.stderr.write("Don't know how to handle user command: " \
                                     + user_cmd)
                    continue
            else:
                # Computer's turn.
                disc_col = self.select_col(self.level)
            # Make move:
            self.game.make_move(disc_col)            
            if self.game.someone_wins():
                self.__someone_wins_msg()
                break
            if self.game.get_available_columns() == []:
                self.show()
                print "Draw game."
                break
            
    def play_game(self):
        '''
        Play game continuously.
        '''
        # Play game.
        play_game = True
        while play_game:
            self.play_one_game()
            while True:
                user_input = raw_input \
                    ("Do you want to play one more game? (y/n): ")
                if user_input == 'y':
                    play_game = True
                    last_player1_type = self.player_type["player1"]
                    last_player2_type = self.player_type["player2"]                    
                    # Initialize a new game.
                    # Need to initialize new game after storing last players'
                    # types, otherwise that info is lost after initialization.
                    self.game = ConnectFour(self.row, self.col)
                    # Switch the order of first moves.
                    self.player_type["player1"] = last_player2_type
                    self.player_type["player2"] = last_player1_type
                    # Switch disc type so that players keep using the same disc.
                    last_player1_disc = self.player_disc["player1"]
                    last_player2_disc = self.player_disc["player2"]
                    self.player_disc["player1"] = last_player2_disc
                    self.player_disc["player2"] = last_player1_disc
                    # Switch player names accordingly.
                    last_player1_name = self.player_name["player1"]
                    last_player2_name = self.player_name["player2"]
                    self.player_name["player1"] = last_player2_name
                    self.player_name["player2"] = last_player1_name
                    break
                if user_input == 'n':
                    play_game = False
                    break
                print "Invalid input!"
                
    def run(self):
        '''
        Play Connect Four games.
        '''
        # Welcoming message.
        print '***************'
        print 'Play Connect 4!'
        print '***************'        
        
        # Ask user for game mode.
        self.__select_mode()
        
        if self.game_mode == 1:
            # Ask user for disc type.
            self.__select_disc()
            # Ask user for difficulty level.
            self.__select_level()
        elif self.game_mode == 2:
            self.player_type["player1"] = 'human'
            self.player_type["player2"] = 'human'
        else:
            pass
        # Ask user for names.
        self.__select_names()
        self.play_game()
           
        # TODO: Advanced features:
        # 1.Ask user what kind of game to play. (7x6 or 8x7 etc..)
        # 5.Make something easier for human than level 2
        
    # TDODO: Better name for 'select_col'.
    def select_col(self, level = 0):
        '''
        Selects a column number based on minimax algorithm.
        '''
        if level == 0:
            # Implement stupid computer by randomly selecting columns.
            random.seed()
            while True:
                selected_col = random.randint(0, self.game.get_col() - 1)
                if selected_col in self.game.get_available_columns():
                    return selected_col
        else:
            # Use "Minimax" algorithm to select a column.
            # Convert to a board representation 'node' that class "minimax"
            # works on.
            if self.game.get_current_player() is 'player1':
                # Player1 is maximizing player.
                node = self.game.convert_board(1, -1, 0)
            else:
                node = self.game.convert_board(-1, 1, 0)             
            return self.minimax.next_move(node, level)
    
    # Helpers:
    def __select_mode(self):
        '''
        Ask user to seelct a game mode.  And store the result into 
        'self.game_mode'.
        Argument: None
        Return:None.
        '''
        while True:
            try:
                print
                print 'Game mode:'
                print '1. One player'
                print '2. Two player2'            
                self.game_mode = int(raw_input("Select game mode (1/2): "))
                if self.game_mode not in [1, 2]:
                    raise ConnectFourCommandError("Error: input must be 1 or 2")
                break
            except ValueError:
                print
                print 'Error: Invalid input!'
                print
            except ConnectFourCommandError, e:
                print
                print e
                print
                
    def __select_disc(self):
        '''
        Ask user to select disc type, and set player type according to
        user's selection.
        Argument: None
        Return: None
        '''
        while True:
            try:
                print
                print "Disc type:"
                print "1. " + self.player_disc["player1"] + " (go first)"
                print "2. " + self.player_disc["player2"]
                selection = raw_input("Select disc: ")
                if selection not in self.player_disc.values():
                    raise ConnectFourCommandError\
                          ("Error: input needs to be either %s or %s " % \
                           (self.player_disc["player1"], \
                            self.player_disc["player2"]))
                break
            except ConnectFourCommandError, e:
                print
                print e
                print
        if selection == self.player_disc["player1"]:
            # Player1 is "human" because player1 goes first.
            self.player_type["player1"] = 'human'
            self.player_type["player2"] = 'computer'
        else:
            # Player2 is "human".
            self.player_type["player2"] = 'human'
            self.player_type["player1"] = 'computer'
        
    def __select_level(self):
        '''
        Ask user to select difficulty level, and save it in "self.level".
        '''
        while True:
            try:
                print
                print 'Difficulty:'
                print '1. Easy'
                print '2. Normal'
                print '3. Hell'
                print '4. Inferno'
                self.level = int(raw_input("Select difficulty (1 - 4): "))
                if self.level not in [1, 2, 3, 4]:
                    raise ConnectFourCommandError("Error: input must be 1 to 4")
                break
            except ValueError:
                print
                print 'Error: Invalid input!'
                print
            except ConnectFourCommandError, e:
                print
                print e
                print
    
    def __select_names(self):
        '''
        Ask users to give names.  And store names into "self.player_name".
        '''        
        if self.game_mode == 1:
            # Only need one name.
            print
            name = raw_input("Enter your name: ")
            if self.player_type["player1"] is "human":
                self.player_name["player1"] = name
                self.player_name["player2"] = "computer"
            else:
                self.player_name["player2"] = name
                self.player_name["player1"] = "computer"
        elif self.game_mode == 2:
            # Need two names.
            print
            name1 = raw_input("Enter first player's name: ")
            print
            name2 = raw_input("Enter second player's name: ")
            self.player_name["player1"] = name1
            self.player_name["player2"] = name2            
        
        
    def __current_player_type(self):
        '''
        Return "human" if current player is human; return "computer" if
        current player is computer.
        Argument: None
        Return: string
        '''
        if self.game.get_current_player() is 'player1':
            if self.player_type["player1"] is 'human':
                print "%s's turn:" % self.player_name["player1"]                
                return "human"
            else:
                return "computer"
        else:
            # current player is "player2".
            if self.player_type["player2"] is 'human':
                print "%s's turn:" % self.player_name["player2"]                
                return "human"
            else:
                return "computer"
            
    def __undo(self):
        '''
        Undo current move.
        '''
        try:
            if self.game_mode == 1:
                # human vs. computer
                # undo twice because we also need to undo computer's move.
                self.game.undo()
                self.game.undo()
            elif self.game_mode == 2:
                # human vs. human
                self.game.undo()
        except ConnectFourError, e:
            print e

    def __load(self):
        '''
        Load a saved game file.
        '''
        # Start a new game
        try:
            self.game = self.game_file.load()
            self.__select_level()
        except IOError:
            print "File doesn't exist."
            print "Please give a valid filename."
        except ConnectFourFileError, e:
            print "Invalid file format."
            print e
            print "Please give another valid file (.c4)."
            
    def __exit_game(self):
        '''
        Ask user what to do when an exit command is given.
        Argument: None.
        Return:
        -- bool: True if exit game, False if not.
        '''
        while True:
            try:
                print
                print 'What do you want:'
                print '1. Resign game.'
                print '2. Draw game.'
                print '3. Abandon game.'
                print '4. Cancel.'
                action = int(raw_input("Select action(1 - 4): "))
                if action not in [1, 2, 3, 4]:
                    raise ConnectFourCommandError("Error: input must be 1 - 4")
                break
            except ValueError:
                print
                print 'Error: Invalid input!'
                print
            except ConnectFourCommandError, e:
                print
                print e
                print
        # TODO: fill in actions for first three cases.
        if action == 1:
            return True
        elif action == 2:
            return True
        elif action == 3:
            return True
        else:
            return False
            

    def __someone_wins_msg(self):
        '''
        Print message at the end of game if anyone wins.
        '''
        winning_msg = "Yeah!  You beat the computer!"
        losing_msg = "You lose :(  Try again."        
        # Display:
        self.show()
        if self.game_mode == 1:
            # Play agains computer.
            if self.game.get_current_player() is 'player1':
                # Last player is player2.
                if self.player_type["player2"] is "human":
                    print winning_msg
                else:
                    # Player2 is computer, and wins.
                    print losing_msg
            else:
                # Last player is player1.
                if self.player_type["player1"] is "human":
                    print winning_msg
                else:
                    # Player1 is computer, and wins.
                    print losing_msg
        elif self.game_mode == 2:
            # human vs. human
            if self.game.get_current_player() is 'player1':
                # Last player is player2.
                print "Congratulations, %s!  You win!" % \
                      self.player_name["player2"]
            else:
                print "Congratulations, %s!  You win!" % \
                      self.player_name["player1"]
        
if __name__ == '__main__':
    s = ConnectFourInterface()
    s.run()

